// Decompiled by JEB v1.4.201311050

package com.dbs.ffi;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract$CommonDataKinds$Phone;
import android.provider.ContactsContract$Contacts;
import android.provider.ContactsContract$Data;
import android.util.Base64;
import android.util.Log;
import com.konylabs.android.KonyMain;
import java.util.Hashtable;
import java.util.Vector;

public class MainActivity extends Activity {
    public MainActivity() {
        super();
    }

    public static void main(String[] args) {
    }

    private String queryContactImage(int imageDataRow) {
        String v5 = null;
        Cursor v6 = KonyMain.getActivityContext().getContentResolver().query(ContactsContract$Data.CONTENT_URI, new String[]{"data15"}, "_id=?", new String[]{Integer.toString(imageDataRow)}, v5);
        byte[] v8 = ((byte[])v5);
        if(v6 != null) {
            if(v6.moveToFirst()) {
                v8 = v6.getBlob(0);
            }

            v6.close();
        }

        if(v8 != null) {
            v5 = Base64.encodeToString(v8, 0);
        }

        return v5;
    }

    public Vector readContacts() {
        String v9;
        String v15;
        String v14;
        Log.e("MyLog", "###########In readContacts");
        KonyMain v10 = KonyMain.getActivityContext();
        Log.e("MyLog", "###########new context" + v10);
        ContentResolver v2 = ((Context)v10).getContentResolver();
        Log.e("MyLog", "########### prahtam" + v2);
        Cursor v11 = v2.query(ContactsContract$Contacts.CONTENT_URI, null, null, null, null);
        Log.e("MyLog", "########### after fetching contact");
        Vector v8 = new Vector();
        if(v11.getCount() <= 0) {
            goto label_40;
        }

        Log.e("MyLog", "########### In if cur==" + v11);
        do {
        label_38:
            if(v11.moveToNext()) {
                goto label_47;
            }

            goto label_40;
        label_47:
            Log.e("MyLog", "########### In while==" + v11);
            v14 = v11.getString(v11.getColumnIndex("_id"));
            Log.e("MyLog", "########### id==" + v14);
            v15 = v11.getString(v11.getColumnIndex("display_name"));
            Log.e("MyLog", "########### name==" + v15);
            try {
                Log.e("MyLog", "########### colIndex name==" + v11.getColumnIndex("display_name"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID==" + v11.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID from contacts==" + v11.getColumnIndex("photo_id"));
                int v18 = v11.getInt(v11.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### uri==" + v18);
                v9 = this.queryContactImage(v18);
                Log.e("MyLog", "########### base64==" + v9);
            }
            catch(Exception v13) {
                v13.printStackTrace();
            }
        }
        while(Integer.parseInt(v11.getString(v11.getColumnIndex("has_phone_number"))) <= 0);

        Log.e("MyLog", "########### in second if==");
        System.out.println("name : " + v15 + ", ID : " + v14);
        Cursor v16 = v2.query(ContactsContract$CommonDataKinds$Phone.CONTENT_URI, null, "contact_id = ?", new String[]{v14}, null);
        System.out.println("After Fetching all phone numbers" + v16);
        while(v16.moveToNext()) {
            Hashtable v12 = new Hashtable();
            String v17 = v16.getString(v16.getColumnIndex("data1"));
            try {
                v12.put("displayName", v15);
                v12.put("phone", v17);
                if(v9 != null) {
                    v12.put("base64", v9);
                }

                Log.e("MyLog", "########### currContact==" + v12);
                Log.e("MyLog", "########### adding contact to allcontact==");
                v8.addElement(v12);
                Log.e("MyLog", "########### after adding contact " + v8);
                continue;
            }
            catch(Exception v13) {
                v13.printStackTrace();
                continue;
            }
        }

        v16.close();
        goto label_38;
    label_40:
        Log.e("MyLog", "########### allContacts==" + v8);
        return v8;
    }
}

