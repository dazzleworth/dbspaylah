// Decompiled by JEB v1.4.201311050

package com.dbs.ffi;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract$CommonDataKinds$Phone;
import android.provider.ContactsContract$Contacts;
import android.provider.ContactsContract$Data;
import android.util.Base64;
import android.util.Log;
import com.konylabs.android.KonyMain;
import java.util.Hashtable;
import java.util.Vector;

public class FilteredContact {
    public FilteredContact() {
        super();
    }

    private String queryContactImage(int imageDataRow) {
        String v5 = null;
        Cursor v6 = KonyMain.getActivityContext().getContentResolver().query(ContactsContract$Data.CONTENT_URI, new String[]{"data15"}, "_id=?", new String[]{Integer.toString(imageDataRow)}, v5);
        byte[] v8 = ((byte[])v5);
        if(v6 != null) {
            if(v6.moveToFirst()) {
                v8 = v6.getBlob(0);
            }

            v6.close();
        }

        if(v8 != null) {
            v5 = Base64.encodeToString(v8, 0);
        }

        return v5;
    }

    public Vector readFilteredContacts(String searchString) {
        String v23;
        Hashtable v18;
        String v15;
        int v24;
        String v21;
        String v20;
        Vector v14 = new Vector();
        ContentResolver v2 = KonyMain.getActivityContext().getContentResolver();
        if(!searchString.matches("-?\\d+(\\.\\d+)?")) {
            goto label_195;
        }

        Log.e("MyLog", "########### in the search by number if");
        Cursor v17 = v2.query(ContactsContract$CommonDataKinds$Phone.CONTENT_URI, new String[]{"_id", "display_name", "photo_id", "data1"}, "data1 like \'%" + searchString + "%\'", null, "display_name COLLATE LOCALIZED ASC");
        Log.e("MyLog", "########### after fetching all contact in if");
        if(v17.getCount() <= 0) {
            goto label_52;
        }

        Log.e("MyLog", "########### In if cur==" + v17);
        while(true) {
            if(v17.moveToNext()) {
                goto label_59;
            }

            goto label_52;
        label_59:
            Log.e("MyLog", "########### In while==" + v17);
            v20 = v17.getString(v17.getColumnIndex("_id"));
            Log.e("MyLog", "########### id==" + v20);
            v21 = v17.getString(v17.getColumnIndex("display_name"));
            Log.e("MyLog", "########### name==" + v21);
            try {
                Log.e("MyLog", "########### colIndex name==" + v17.getColumnIndex("display_name"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID==" + v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID from contacts==" + v17.getColumnIndex("photo_id"));
                v24 = v17.getInt(v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### uri==" + v24);
                v15 = this.queryContactImage(v24);
                Log.e("MyLog", "########### base64==" + v15);
            }
            catch(Exception v19) {
                v19.printStackTrace();
            }

            Log.e("MyLog", "########### in second if==");
            System.out.println("name : " + v21 + ", ID : " + v20);
            v18 = new Hashtable();
            v23 = v17.getString(v17.getColumnIndex("data1"));
            try {
                v18.put("displayName", v21);
                v18.put("phone", v23);
                if(v15 != null) {
                    v18.put("base64", v15);
                }

                Log.e("MyLog", "########### currContact==" + v18);
                Log.e("MyLog", "########### adding contact to allcontact==");
                v14.addElement(v18);
                Log.e("MyLog", "########### after adding contact " + v14);
                continue;
            }
            catch(Exception v19) {
                v19.printStackTrace();
                continue;
            }
        }

    label_195:
        v17 = v2.query(ContactsContract$Contacts.CONTENT_URI, new String[]{"_id", "display_name", "photo_id", "has_phone_number"}, "display_name like \'%" + searchString + "%\'", null, "display_name COLLATE LOCALIZED ASC");
        Log.e("MyLog", "########### prahtam" + v2);
        Log.e("MyLog", "########### after fetching contact");
        if(v17.getCount() <= 0) {
            goto label_52;
        }

        Log.e("MyLog", "########### In if cur==" + v17);
        do {
        label_238:
            if(!v17.moveToNext()) {
                goto label_52;
            }

            Log.e("MyLog", "########### In while==" + v17);
            v20 = v17.getString(v17.getColumnIndex("_id"));
            Log.e("MyLog", "########### id==" + v20);
            v21 = v17.getString(v17.getColumnIndex("display_name"));
            Log.e("MyLog", "########### name==" + v21);
            try {
                Log.e("MyLog", "########### colIndex name==" + v17.getColumnIndex("display_name"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID==" + v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID from contacts==" + v17.getColumnIndex("photo_id"));
                v24 = v17.getInt(v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### uri==" + v24);
                v15 = this.queryContactImage(v24);
                Log.e("MyLog", "########### base64==" + v15);
            }
            catch(Exception v19) {
                v19.printStackTrace();
            }
        }
        while(Integer.parseInt(v17.getString(v17.getColumnIndex("has_phone_number"))) <= 0);

        Log.e("MyLog", "########### in second if==");
        System.out.println("name : " + v21 + ", ID : " + v20);
        Cursor v22 = v2.query(ContactsContract$CommonDataKinds$Phone.CONTENT_URI, null, "contact_id = ?", new String[]{v20}, null);
        System.out.println("After Fetching all phone numbers" + v22);
        while(v22.moveToNext()) {
            v18 = new Hashtable();
            v23 = v22.getString(v22.getColumnIndex("data1"));
            try {
                v18.put("displayName", v21);
                v18.put("phone", v23);
                if(v15 != null) {
                    v18.put("base64", v15);
                }

                Log.e("MyLog", "########### currContact==" + v18);
                Log.e("MyLog", "########### adding contact to allcontact==");
                v14.addElement(v18);
                Log.e("MyLog", "########### after adding contact " + v14);
                continue;
            }
            catch(Exception v19) {
                v19.printStackTrace();
                continue;
            }
        }

        v22.close();
        goto label_238;
    label_52:
        Log.e("MyLog", "########### allContacts==" + v14);
        return v14;
    }
}

