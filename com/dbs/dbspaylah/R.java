// Decompiled by JEB v1.4.201311050

package com.dbs.dbspaylah;

public final class R {
    public final class anim {
        public static final int accelerate_interpolator = 2130968576;
        public static final int bounce = 2130968577;
        public static final int cycle_interpolator = 2130968578;
        public static final int decelerate_interpolator = 2130968579;
        public static final int fancy_anim = 2130968580;
        public static final int grow_fade_from_bottomleft = 2130968581;
        public static final int grow_fade_from_bottomright = 2130968582;
        public static final int grow_fade_from_center = 2130968583;
        public static final int grow_fade_from_topright = 2130968584;
        public static final int grow_from_center = 2130968585;
        public static final int left_in = 2130968586;
        public static final int left_out = 2130968587;
        public static final int right_in = 2130968588;
        public static final int right_out = 2130968589;
        public static final int shake = 2130968590;
        public static final int shrink_fade_out_from_bottom = 2130968591;
        public static final int slide_from_bottom = 2130968592;
        public static final int slide_in_down = 2130968593;
        public static final int slide_in_left = 2130968594;
        public static final int slide_in_right = 2130968595;
        public static final int slide_in_top = 2130968596;
        public static final int slide_in_up = 2130968597;
        public static final int split = 2130968598;
        public static final int split_in = 2130968599;
        public static final int split_out = 2130968600;
        public static final int still = 2130968601;

        public anim() {
            super();
        }
    }

    public final class attr {
        public attr() {
            super();
        }
    }

    public final class drawable {
        public static final int about = 2130837504;
        public static final int account_settings = 2130837505;
        public static final int add_profile_pic = 2130837506;
        public static final int android = 2130837507;
        public static final int app_icon = 2130837508;
        public static final int arrow = 2130837509;
        public static final int back = 2130837510;
        public static final int back_hover = 2130837511;
        public static final int bday = 2130837512;
        public static final int bday_transparent = 2130837513;
        public static final int benefits_1 = 2130837514;
        public static final int benefits_2 = 2130837515;
        public static final int benefits_3 = 2130837516;
        public static final int benifits = 2130837517;
        public static final int benifits2 = 2130837518;
        public static final int benifits3 = 2130837519;
        public static final int blackdot = 2130837520;
        public static final int btn_check_off = 2130837521;
        public static final int btn_check_on = 2130837522;
        public static final int cab = 2130837523;
        public static final int cab_transparent = 2130837524;
        public static final int check_active = 2130837525;
        public static final int check_deactivated = 2130837526;
        public static final int checkbox_off = 2130837527;
        public static final int checkbox_on = 2130837528;
        public static final int close = 2130837529;
        public static final int close_hover = 2130837530;
        public static final int combo_box_focus_skin = 2130837531;
        public static final int combo_box_normal_skin = 2130837532;
        public static final int deepavali = 2130837533;
        public static final int deepavali_transparent = 2130837534;
        public static final int default_profile_pic = 2130837535;
        public static final int dot_normal = 2130837536;
        public static final int dot_trnsprt = 2130837537;
        public static final int down_arrow = 2130837538;
        public static final int drink = 2130837539;
        public static final int drink_transparent = 2130837540;
        public static final int food = 2130837541;
        public static final int food_transparent = 2130837542;
        public static final int gong = 2130837543;
        public static final int gong2 = 2130837544;
        public static final int gong2_transparent = 2130837545;
        public static final int gong_transparent = 2130837546;
        public static final int hariraya = 2130837547;
        public static final int hariraya_transparent = 2130837548;
        public static final int highlighted_menu_icon = 2130837549;
        public static final int hny = 2130837550;
        public static final int hny_transparent = 2130837551;
        public static final int home = 2130837552;
        public static final int homepage_bg = 2130837553;
        public static final int homepage_bg_480 = 2130837554;
        public static final int ic_cal_icon = 2130837555;
        public static final int ic_cal_next = 2130837556;
        public static final int ic_cal_prev = 2130837557;
        public static final int ic_himagestrip_left = 2130837558;
        public static final int ic_himagestrip_right = 2130837559;
        public static final int ic_image_error_icon = 2130837560;
        public static final int ic_image_loading_icon = 2130837561;
        public static final int ic_list_normal_icon = 2130837562;
        public static final int ic_map_cancelicon = 2130837563;
        public static final int ic_map_marker_icon = 2130837564;
        public static final int ic_map_marker_icon_trans = 2130837565;
        public static final int ic_map_righticon = 2130837566;
        public static final int ic_popup_list_icon = 2130837567;
        public static final int icon = 2130837568;
        public static final int icon_maxedout = 2130837569;
        public static final int icon_refund = 2130837570;
        public static final int icon_sendback = 2130837571;
        public static final int icon_top_up = 2130837572;
        public static final int in = 2130837573;
        public static final int kony_logo = 2130837574;
        public static final int logo = 2130837575;
        public static final int logout = 2130837576;
        public static final int maxedout = 2130837577;
        public static final int mchallenge = 2130837578;
        public static final int menu = 2130837579;
        public static final int menu_bg = 2130837580;
        public static final int menu_bg_480 = 2130837581;
        public static final int menu_bg_960 = 2130837582;
        public static final int menu_hover = 2130837583;
        public static final int movie = 2130837584;
        public static final int movie_transparent = 2130837585;
        public static final int no_sticker = 2130837586;
        public static final int out = 2130837587;
        public static final int overlay_background = 2130837588;
        public static final int overlay_background960 = 2130837589;
        public static final int page_indicator_active = 2130837590;
        public static final int page_indicator_inactive = 2130837591;
        public static final int pagination_dot = 2130837592;
        public static final int party = 2130837593;
        public static final int party_transparent = 2130837594;
        public static final int paylahlogo = 2130837595;
        public static final int pin_img = 2130837596;
        public static final int plash1136 = 2130837597;
        public static final int plash1280 = 2130837598;
        public static final int plash800a = 2130837599;
        public static final int plash960 = 2130837600;
        public static final int red_dot = 2130837601;
        public static final int red_tick = 2130837602;
        public static final int refund = 2130837603;
        public static final int send_back = 2130837604;
        public static final int send_money_focus = 2130837605;
        public static final int send_money_focus_480 = 2130837606;
        public static final int send_money_home = 2130837607;
        public static final int send_money_home_focus = 2130837608;
        public static final int send_money_menu_focus = 2130837609;
        public static final int send_money_menu_hover = 2130837610;
        public static final int send_money_menu_normal = 2130837611;
        public static final int send_money_normal = 2130837612;
        public static final int send_money_normal_480 = 2130837613;
        public static final int sign1 = 2130837614;
        public static final int sign480 = 2130837615;
        public static final int splash = 2130837616;
        public static final int sun = 2130837617;
        public static final int th3 = 2130837618;
        public static final int th_icons1 = 2130837619;
        public static final int th_icons4 = 2130837620;
        public static final int thank_you = 2130837621;
        public static final int thank_you_transparent = 2130837622;
        public static final int tick = 2130837623;
        public static final int token = 2130837624;
        public static final int top_1 = 2130837625;
        public static final int top_1480 = 2130837626;
        public static final int top_1540 = 2130837627;
        public static final int top_2 = 2130837628;
        public static final int top_2480 = 2130837629;
        public static final int top_2540 = 2130837630;
        public static final int top_gree = 2130837631;
        public static final int top_green = 2130837632;
        public static final int top_red = 2130837633;
        public static final int top_up = 2130837634;
        public static final int topup_wallet = 2130837635;
        public static final int trans_history = 2130837636;
        public static final int transparent = 2130837637;
        public static final int transparent_dot = 2130837638;
        public static final int transparent_small = 2130837639;
        public static final int trns_icon = 2130837640;
        public static final int wallet_icon = 2130837641;
        public static final int wallet_icon480 = 2130837642;
        public static final int wallet_icon_2 = 2130837643;
        public static final int wallet_icon_2480 = 2130837644;
        public static final int wallet_icon_480 = 2130837645;
        public static final int wallet_to_mobile_image_one = 2130837646;
        public static final int wallet_to_mobile_image_one480 = 2130837647;
        public static final int wallet_to_mobile_image_one800 = 2130837648;
        public static final int wallet_to_mobile_image_three = 2130837649;
        public static final int wallet_to_mobile_image_three480 = 2130837650;
        public static final int wallet_to_mobile_image_three800 = 2130837651;
        public static final int wallet_to_mobile_image_two = 2130837652;
        public static final int wallet_to_mobile_image_two480 = 2130837653;
        public static final int wallet_to_mobile_image_two800 = 2130837654;
        public static final int wheel_bg = 2130837655;
        public static final int white = 2130837656;
        public static final int withdraw = 2130837657;
        public static final int xmas = 2130837658;
        public static final int xmas_transparent = 2130837659;

        public drawable() {
            super();
        }
    }

    public final class fonts {
        public static final int anti_aliasing = 2131034115;
        public static final int font_size = 2131034112;
        public static final int font_style = 2131034114;
        public static final int font_weight = 2131034113;

        public fonts() {
            super();
        }
    }

    public final class id {
        public static final int tabline = 2131361792;
        public static final int wheelBkgnd = 2131361793;
        public static final int wheelFocusDrawable = 2131361794;

        public id() {
            super();
        }
    }

    public final class layout {
        public static final int tabview = 2130903040;
        public static final int wheel_item = 2130903041;

        public layout() {
            super();
        }
    }

    public final class location {
        public static final int accuracy_within_time = 2131099650;
        public static final int min_distance = 2131099649;
        public static final int min_time = 2131099648;

        public location() {
            super();
        }
    }

    public final class net {
        public static final int conn_per_route = 2131165186;
        public static final int conn_total = 2131165185;
        public static final int enable_conn_pool = 2131165184;
        public static final int enable_gzip = 2131165187;
        public static final int expect_100_continue = 2131165188;

        public net() {
            super();
        }
    }

    public final class string {
        public static final int app_name = 2131230741;
        public static final int notify_new_regid = 2131230720;
        public static final int notify_new_regid_clear = 2131230727;
        public static final int notify_new_regid_desc = 2131230723;
        public static final int notify_new_regid_icon = 2131230721;
        public static final int notify_new_regid_lights = 2131230726;
        public static final int notify_new_regid_sound = 2131230724;
        public static final int notify_new_regid_title = 2131230722;
        public static final int notify_new_regid_vibrate = 2131230725;
        public static final int notify_push_msg = 2131230728;
        public static final int notify_push_msg_clear = 2131230740;
        public static final int notify_push_msg_default_desc = 2131230734;
        public static final int notify_push_msg_default_title = 2131230731;
        public static final int notify_push_msg_desc_from_payload = 2131230735;
        public static final int notify_push_msg_desc_keys = 2131230736;
        public static final int notify_push_msg_icon = 2131230730;
        public static final int notify_push_msg_lights = 2131230739;
        public static final int notify_push_msg_notifications_count = 2131230729;
        public static final int notify_push_msg_sound = 2131230737;
        public static final int notify_push_msg_title_from_payload = 2131230732;
        public static final int notify_push_msg_title_keys = 2131230733;
        public static final int notify_push_msg_vibrate = 2131230738;

        public string() {
            super();
        }
    }

    public final class style {
        public static final int Center = 2131296261;
        public static final int DropDownLeftUp = 2131296256;
        public static final int DropDownRightUp = 2131296257;
        public static final int DropUpDown = 2131296258;
        public static final int SlideInDown = 2131296260;
        public static final int SlideInLeft = 2131296262;
        public static final int SlideInRight = 2131296263;
        public static final int SlideInUp = 2131296259;

        public style() {
            super();
        }
    }

    public R() {
        super();
    }
}

