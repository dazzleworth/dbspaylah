// Decompiled by JEB v1.4.201311050

package com.dbs.rotateimage;

import android.graphics.Bitmap;
import android.graphics.Bitmap$CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory$Options;
import android.graphics.Matrix;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

public class RotateImageFFI {
    public RotateImageFFI() {
        super();
    }

    public static String bitmapTobase64(Bitmap b) {
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In the convert to base64 function");
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Bitmap in convert to base64");
        ByteArrayOutputStream v0 = new ByteArrayOutputStream();
        b.compress(Bitmap$CompressFormat.PNG, 100, ((OutputStream)v0));
        String v3 = Base64.encodeToString(v0.toByteArray(), 0);
        Log.e("rotate", "Rotated base64" + v3);
        b.recycle();
        return v3;
    }

    public static int calculateInSampleSize(BitmapFactory$Options options, int reqWidth, int reqHeight) {
        int v2 = options.outHeight;
        int v4 = options.outWidth;
        int v3 = 1;
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ original height ==" + v2);
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ original height ==" + v4);
        if(v2 > reqHeight) {
            goto label_17;
        }

        if(v4 <= reqWidth) {
            goto label_23;
        }

    label_17:
        int v0 = v2 / 5;
        int v1 = v4 / 5;
    label_19:
        if(v0 / v3 <= reqHeight) {
            goto label_23;
        }

        if(v1 / v3 > reqWidth) {
            goto label_24;
        }

        goto label_23;
    label_24:
        v3 *= 2;
        goto label_19;
    label_23:
        return v3;
    }

    public static Bitmap rotate(Bitmap b) {
        float v3 = 2f;
        int v8 = 90;
        if(b != null) {
            Matrix v5 = new Matrix();
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In the rotate Image function");
            v5.setRotate(((float)v8), (((float)b.getWidth())) / v3, (((float)b.getHeight())) / v3);
            try {
                Bitmap v7 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), v5, true);
                Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Creating  bitmap in rotate function");
                if(b != v7) {
                    b.recycle();
                    b = v7;
                }
            }
            catch(OutOfMemoryError v9) {
                throw v9;
            }
        }

        return b;
    }

    public static String rotateFFI(String base64) {
        String v4;
        String v2 = Build.MANUFACTURER;
        if((v2.toLowerCase().contains("samsung")) || (v2.toLowerCase().contains("sony"))) {
            byte[] v1 = Base64.decode(base64, 0);
            BitmapFactory$Options v3 = new BitmapFactory$Options();
            v3.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(v1, 0, v1.length, v3);
            v3.inSampleSize = RotateImageFFI.calculateInSampleSize(v3, 320, 240);
            v3.inJustDecodeBounds = false;
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Creating initial bitmap");
            Bitmap v0 = BitmapFactory.decodeByteArray(v1, 0, v1.length, v3);
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Calling rotate image function");
            Bitmap v5 = RotateImageFFI.rotate(v0);
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ After rotate bitmap function");
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Calling convert to base64 function");
            v4 = RotateImageFFI.bitmapTobase64(v5);
            v0.recycle();
            v5.recycle();
        }
        else {
            v4 = null;
        }

        return v4;
    }
}

