// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class AESTest {
    private static final String ALGO = "AES/CBC/PKCS5Padding";

    public AESTest() {
        super();
    }

    public static String decryptWithKey(String encryptedData, String passphrase) {
        String v7;
        String v8;
        try {
            IvParameterSpec v4 = new IvParameterSpec("1234567890123456".getBytes());
            Key v6 = AESTest.generateKey(passphrase);
            Cipher v0 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            v0.init(2, v6, ((AlgorithmParameterSpec)v4));
            v8 = new String(v0.doFinal(Base64.decodeBase64(encryptedData)));
            v7 = v8;
            goto label_13;
        }
        catch(Exception v3) {
            System.out.println("Any other Exception: " + v3.getMessage());
        }
        catch(BadPaddingException v3_1) {
            System.out.println("BadPaddingException: " + v3_1.getMessage());
        }
        catch(IllegalBlockSizeException v3_2) {
            System.out.println("IllegalBlockSizeException: " + v3_2.getMessage());
        }
        catch(InvalidKeyException v3_3) {
            System.out.println("InvalidKeyException: " + v3_3.getMessage());
        }
        catch(NoSuchPaddingException v3_4) {
            System.out.println("NoSuchPaddingException: " + v3_4.getMessage());
        }
        catch(NoSuchAlgorithmException v3_5) {
            System.out.println("NoSuchAlgorithmException: " + v3_5.getMessage());
        }

        v8 = v7;
    label_13:
        return v8;
    }

    public static String encryptWithKey(String Data, String passphrase) {
        String v4;
        String v3;
        try {
            IvParameterSpec v5 = new IvParameterSpec("1234567890123456".getBytes());
            Key v7 = AESTest.generateKey(passphrase);
            Cipher v0 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            v0.init(1, v7, ((AlgorithmParameterSpec)v5));
            v3 = Base64.encodeBase64String(v0.doFinal(Data.getBytes("UTF-8")));
            v4 = v3;
            goto label_14;
        }
        catch(Exception v1) {
            System.out.println("Any other Exception: " + v1.getMessage());
        }
        catch(BadPaddingException v1_1) {
            System.out.println("BadPaddingException: " + v1_1.getMessage());
        }
        catch(IllegalBlockSizeException v1_2) {
            System.out.println("IllegalBlockSizeException: " + v1_2.getMessage());
        }
        catch(InvalidKeyException v1_3) {
            System.out.println("InvalidKeyException: " + v1_3.getMessage());
        }
        catch(NoSuchPaddingException v1_4) {
            System.out.println("NoSuchPaddingException: " + v1_4.getMessage());
        }
        catch(NoSuchAlgorithmException v1_5) {
            System.out.println("NoSuchAlgorithmException: " + v1_5.getMessage());
        }
        catch(UnsupportedEncodingException v1_6) {
            System.out.println("UnsupportedEncodingException: " + v1_6.getMessage());
        }

        v4 = v3;
    label_14:
        return v4;
    }

    private static Key generateKey(String passphrase) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return new SecretKeySpec(passphrase.getBytes(), "AES");
    }

    public static void main(String[] args) throws Exception {
    }
}

