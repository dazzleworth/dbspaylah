// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.math.BigInteger;
import org.apache.commons.codec.BinaryDecoder;
import org.apache.commons.codec.BinaryEncoder;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;

public class Base64 implements BinaryDecoder, BinaryEncoder {
    static final byte[] CHUNK_SEPARATOR = null;
    static final int CHUNK_SIZE = 76;
    private static final byte[] DECODE_TABLE = null;
    private static final int DEFAULT_BUFFER_RESIZE_FACTOR = 2;
    private static final int DEFAULT_BUFFER_SIZE = 8192;
    private static final int MASK_6BITS = 63;
    private static final int MASK_8BITS = 255;
    private static final byte PAD = 61;
    private static final byte[] STANDARD_ENCODE_TABLE;
    private static final byte[] URL_SAFE_ENCODE_TABLE;
    private byte[] buffer;
    private int currentLinePos;
    private final int decodeSize;
    private final int encodeSize;
    private final byte[] encodeTable;
    private boolean eof;
    private final int lineLength;
    private final byte[] lineSeparator;
    private int modulus;
    private int pos;
    private int readPos;
    private int x;

    static  {
        Base64.CHUNK_SEPARATOR = new byte[]{13, 10};
        Base64.STANDARD_ENCODE_TABLE = new byte[]{65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        Base64.URL_SAFE_ENCODE_TABLE = new byte[]{65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        byte[] v0 = new byte[123];
        v0[0] = -1;
        v0[1] = -1;
        v0[2] = -1;
        v0[3] = -1;
        v0[4] = -1;
        v0[5] = -1;
        v0[6] = -1;
        v0[7] = -1;
        v0[8] = -1;
        v0[9] = -1;
        v0[10] = -1;
        v0[11] = -1;
        v0[12] = -1;
        v0[13] = -1;
        v0[14] = -1;
        v0[15] = -1;
        v0[16] = -1;
        v0[17] = -1;
        v0[18] = -1;
        v0[19] = -1;
        v0[20] = -1;
        v0[21] = -1;
        v0[22] = -1;
        v0[23] = -1;
        v0[24] = -1;
        v0[25] = -1;
        v0[26] = -1;
        v0[27] = -1;
        v0[28] = -1;
        v0[29] = -1;
        v0[30] = -1;
        v0[31] = -1;
        v0[32] = -1;
        v0[33] = -1;
        v0[34] = -1;
        v0[35] = -1;
        v0[36] = -1;
        v0[37] = -1;
        v0[38] = -1;
        v0[39] = -1;
        v0[40] = -1;
        v0[41] = -1;
        v0[42] = -1;
        v0[43] = 62;
        v0[44] = -1;
        v0[45] = 62;
        v0[46] = -1;
        v0[47] = 63;
        v0[48] = 52;
        v0[49] = 53;
        v0[50] = 54;
        v0[51] = 55;
        v0[52] = 56;
        v0[53] = 57;
        v0[54] = 58;
        v0[55] = 59;
        v0[56] = 60;
        v0[57] = 61;
        v0[58] = -1;
        v0[59] = -1;
        v0[60] = -1;
        v0[61] = -1;
        v0[62] = -1;
        v0[63] = -1;
        v0[64] = -1;
        v0[66] = 1;
        v0[67] = 2;
        v0[68] = 3;
        v0[69] = 4;
        v0[70] = 5;
        v0[71] = 6;
        v0[72] = 7;
        v0[73] = 8;
        v0[74] = 9;
        v0[75] = 10;
        v0[76] = 11;
        v0[77] = 12;
        v0[78] = 13;
        v0[79] = 14;
        v0[80] = 15;
        v0[81] = 16;
        v0[82] = 17;
        v0[83] = 18;
        v0[84] = 19;
        v0[85] = 20;
        v0[86] = 21;
        v0[87] = 22;
        v0[88] = 23;
        v0[89] = 24;
        v0[90] = 25;
        v0[91] = -1;
        v0[92] = -1;
        v0[93] = -1;
        v0[94] = -1;
        v0[95] = 63;
        v0[96] = -1;
        v0[97] = 26;
        v0[98] = 27;
        v0[99] = 28;
        v0[100] = 29;
        v0[101] = 30;
        v0[102] = 31;
        v0[103] = 32;
        v0[104] = 33;
        v0[105] = 34;
        v0[106] = 35;
        v0[107] = 36;
        v0[108] = 37;
        v0[109] = 38;
        v0[110] = 39;
        v0[111] = 40;
        v0[112] = 41;
        v0[113] = 42;
        v0[114] = 43;
        v0[115] = 44;
        v0[116] = 45;
        v0[117] = 46;
        v0[118] = 47;
        v0[119] = 48;
        v0[120] = 49;
        v0[121] = 50;
        v0[122] = 51;
        Base64.DECODE_TABLE = v0;
    }

    public Base64() {
        this(false);
    }

    public Base64(boolean urlSafe) {
        this(76, Base64.CHUNK_SEPARATOR, urlSafe);
    }

    public Base64(int lineLength) {
        this(lineLength, Base64.CHUNK_SEPARATOR);
    }

    public Base64(int lineLength, byte[] lineSeparator) {
        this(lineLength, lineSeparator, false);
    }

    public Base64(int lineLength, byte[] lineSeparator, boolean urlSafe) {
        byte[] v1_1;
        int v1;
        super();
        if(lineSeparator == null) {
            lineSeparator = Base64.CHUNK_SEPARATOR;
        }

        if(0 > 0) {
            v1 = 0;
        }
        else {
            v1 = 0;
        }

        this.lineLength = v1;
        this.lineSeparator = new byte[lineSeparator.length];
        System.arraycopy(lineSeparator, 0, this.lineSeparator, 0, lineSeparator.length);
        if(0 > 0) {
            this.encodeSize = lineSeparator.length + 4;
        }
        else {
            this.encodeSize = 4;
        }

        this.decodeSize = this.encodeSize - 1;
        if(Base64.containsBase64Byte(lineSeparator)) {
            throw new IllegalArgumentException("lineSeperator must not contain base64 characters: [" + StringUtils.newStringUtf8(lineSeparator) + "]");
        }

        if(urlSafe) {
            v1_1 = Base64.URL_SAFE_ENCODE_TABLE;
        }
        else {
            v1_1 = Base64.STANDARD_ENCODE_TABLE;
        }

        this.encodeTable = v1_1;
    }

    int avail() {
        int v0;
        if(this.buffer != null) {
            v0 = this.pos - this.readPos;
        }
        else {
            v0 = 0;
        }

        return v0;
    }

    private static boolean containsBase64Byte(byte[] arrayOctet) {
        int v0 = 0;
    label_1:
        if(v0 < arrayOctet.length) {
            goto label_5;
        }

        goto label_3;
    label_5:
        if(!Base64.isBase64(arrayOctet[v0])) {
            goto label_10;
        }

        boolean v1 = true;
        goto label_4;
    label_10:
        ++v0;
        goto label_1;
    label_3:
        v1 = false;
    label_4:
        return v1;
    }

    public byte[] decode(String pArray) {
        return this.decode(StringUtils.getBytesUtf8(pArray));
    }

    public byte[] decode(byte[] pArray) {
        this.reset();
        if(pArray == null) {
            goto label_5;
        }

        if(pArray.length != 0) {
            goto label_7;
        }

        goto label_5;
    label_7:
        byte[] v0 = new byte[((int)(((long)(pArray.length * 3 / 4))))];
        this.setInitialBuffer(v0, 0, v0.length);
        this.decode(pArray, 0, pArray.length);
        this.decode(pArray, 0, -1);
        byte[] v3 = new byte[this.pos];
        this.readResults(v3, 0, v3.length);
        goto label_6;
    label_5:
        v3 = pArray;
    label_6:
        return v3;
    }

    public Object decode(Object pObject) throws DecoderException {
        byte[] v0;
        if((pObject instanceof byte[])) {
            v0 = this.decode(((byte[])pObject));
        }
        else if((pObject instanceof String)) {
            v0 = this.decode(((String)pObject));
        }
        else {
            goto label_8;
        }

        return v0;
    label_8:
        throw new DecoderException("Parameter supplied to Base64 decode is not a byte[] or a String");
    }

    void decode(byte[] in, int inPos, int inAvail) {
        int v5;
        byte[] v4_1;
        if(!this.eof) {
            if(inAvail < 0) {
                this.eof = true;
            }

            int v1 = 0;
            int v2;
            for(v2 = inPos; v1 < inAvail; v2 = inPos) {
                if(this.buffer == null || this.buffer.length - this.pos < this.decodeSize) {
                    this.resizeBuffer();
                }

                inPos = v2 + 1;
                int v0 = in[v2];
                if(v0 != 61) {
                    goto label_48;
                }

                this.eof = true;
                goto label_10;
            label_48:
                if(v0 >= 0 && v0 < Base64.DECODE_TABLE.length) {
                    int v3 = Base64.DECODE_TABLE[v0];
                    if(v3 >= 0) {
                        int v4 = this.modulus + 1;
                        this.modulus = v4;
                        this.modulus = v4 % 4;
                        this.x = (this.x << 6) + v3;
                        if(this.modulus == 0) {
                            v4_1 = this.buffer;
                            v5 = this.pos;
                            this.pos = v5 + 1;
                            v4_1[v5] = ((byte)(this.x >> 16 & 255));
                            v4_1 = this.buffer;
                            v5 = this.pos;
                            this.pos = v5 + 1;
                            v4_1[v5] = ((byte)(this.x >> 8 & 255));
                            v4_1 = this.buffer;
                            v5 = this.pos;
                            this.pos = v5 + 1;
                            v4_1[v5] = ((byte)(this.x & 255));
                        }
                    }
                }

                ++v1;
            }

        label_10:
            if(!this.eof) {
                return;
            }

            if(this.modulus == 0) {
                return;
            }

            this.x <<= 6;
            switch(this.modulus) {
                case 2: {
                    goto label_20;
                }
                case 3: {
                    goto label_95;
                }
            }

            return;
        label_20:
            this.x <<= 6;
            v4_1 = this.buffer;
            v5 = this.pos;
            this.pos = v5 + 1;
            v4_1[v5] = ((byte)(this.x >> 16 & 255));
            return;
        label_95:
            v4_1 = this.buffer;
            v5 = this.pos;
            this.pos = v5 + 1;
            v4_1[v5] = ((byte)(this.x >> 16 & 255));
            v4_1 = this.buffer;
            v5 = this.pos;
            this.pos = v5 + 1;
            v4_1[v5] = ((byte)(this.x >> 8 & 255));
        }
    }

    public static byte[] decodeBase64(String base64String) {
        return new Base64().decode(base64String);
    }

    public static byte[] decodeBase64(byte[] base64Data) {
        return new Base64().decode(base64Data);
    }

    public static BigInteger decodeInteger(byte[] pArray) {
        return new BigInteger(1, Base64.decodeBase64(pArray));
    }

    static byte[] discardWhitespace(byte[] data) {
        byte[] v2 = new byte[data.length];
        int v0 = 0;
        int v3;
        for(v3 = 0; v3 < data.length; ++v3) {
            switch(data[v3]) {
                case 9: 
                case 10: 
                case 13: 
                case 32: {
                    break;
                }
                default: {
                    v2[v0] = data[v3];
                    ++v0;
                    break;
                }
            }
        }

        byte[] v4 = new byte[v0];
        System.arraycopy(v2, 0, v4, 0, v0);
        return v4;
    }

    public byte[] encode(byte[] pArray) {
        byte[] v0;
        this.reset();
        if(pArray == null || pArray.length == 0) {
            v0 = pArray;
        }
        else {
            v0 = new byte[((int)Base64.getEncodeLength(pArray, this.lineLength, this.lineSeparator))];
            this.setInitialBuffer(v0, 0, v0.length);
            this.encode(pArray, 0, pArray.length);
            this.encode(pArray, 0, -1);
            if(this.buffer != v0) {
                this.readResults(v0, 0, v0.length);
            }

            if(!this.isUrlSafe()) {
                goto label_6;
            }

            if(this.pos >= v0.length) {
                goto label_6;
            }

            byte[] v3 = new byte[this.pos];
            System.arraycopy(v0, 0, v3, 0, this.pos);
            v0 = v3;
        }

    label_6:
        return v0;
    }

    public Object encode(Object pObject) throws EncoderException {
        if(!(pObject instanceof byte[])) {
            throw new EncoderException("Parameter supplied to Base64 encode is not a byte[]");
        }

        return this.encode(((byte[])pObject));
    }

    void encode(byte[] in, int inPos, int inAvail) {
        byte v8 = 61;
        if(!this.eof) {
            goto label_5;
        }

        return;
    label_5:
        if(inAvail >= 0) {
            goto label_108;
        }

        this.eof = true;
        if(this.buffer == null) {
            goto label_16;
        }

        if(this.buffer.length - this.pos >= this.encodeSize) {
            goto label_17;
        }

    label_16:
        this.resizeBuffer();
    label_17:
        switch(this.modulus) {
            case 1: {
                goto label_35;
            }
            case 2: {
                goto label_69;
            }
        }

        goto label_19;
    label_69:
        byte[] v3 = this.buffer;
        int v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x >> 10 & 63];
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x >> 4 & 63];
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x << 2 & 63];
        if(this.encodeTable != Base64.STANDARD_ENCODE_TABLE) {
            goto label_19;
        }

        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = v8;
        goto label_19;
    label_35:
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x >> 2 & 63];
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x << 4 & 63];
        if(this.encodeTable != Base64.STANDARD_ENCODE_TABLE) {
            goto label_19;
        }

        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = v8;
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = v8;
    label_19:
        if(this.lineLength <= 0) {
            return;
        }

        if(this.pos <= 0) {
            return;
        }

        System.arraycopy(this.lineSeparator, 0, this.buffer, this.pos, this.lineSeparator.length);
        this.pos += this.lineSeparator.length;
        return;
    label_108:
        int v1 = 0;
        int v2 = inPos;
    label_110:
        if(v1 < inAvail) {
            goto label_113;
        }

        return;
    label_113:
        if(this.buffer == null) {
            goto label_121;
        }

        if(this.buffer.length - this.pos >= this.encodeSize) {
            goto label_122;
        }

    label_121:
        this.resizeBuffer();
    label_122:
        int v3_1 = this.modulus + 1;
        this.modulus = v3_1;
        this.modulus = v3_1 % 3;
        inPos = v2 + 1;
        int v0 = in[v2];
        if(v0 >= 0) {
            goto label_131;
        }

        v0 += 256;
    label_131:
        this.x = (this.x << 8) + v0;
        if(this.modulus != 0) {
            goto label_196;
        }

        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x >> 18 & 63];
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x >> 12 & 63];
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x >> 6 & 63];
        v3 = this.buffer;
        v4 = this.pos;
        this.pos = v4 + 1;
        v3[v4] = this.encodeTable[this.x & 63];
        this.currentLinePos += 4;
        if(this.lineLength <= 0) {
            goto label_196;
        }

        if(this.lineLength > this.currentLinePos) {
            goto label_196;
        }

        System.arraycopy(this.lineSeparator, 0, this.buffer, this.pos, this.lineSeparator.length);
        this.pos += this.lineSeparator.length;
        this.currentLinePos = 0;
    label_196:
        ++v1;
        v2 = inPos;
        goto label_110;
    }

    public static byte[] encodeBase64(byte[] binaryData) {
        return Base64.encodeBase64(binaryData, false);
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked) {
        return Base64.encodeBase64(binaryData, isChunked, false);
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked, boolean urlSafe) {
        return Base64.encodeBase64(binaryData, isChunked, urlSafe, 2147483647);
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked, boolean urlSafe, int maxResultSize) {
        if(binaryData == null) {
            goto label_3;
        }

        if(binaryData.length != 0) {
            goto label_4;
        }

        goto label_3;
    label_4:
        long v1 = Base64.getEncodeLength(binaryData, 76, Base64.CHUNK_SEPARATOR);
        if(v1 > (((long)maxResultSize))) {
            throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + v1 + ") than the specified maxium size of " + maxResultSize);
        }

        if(!isChunked) {
            goto label_22;
        }

        Base64 v0 = new Base64(urlSafe);
        goto label_20;
    label_22:
        v0 = new Base64(0, Base64.CHUNK_SEPARATOR, urlSafe);
    label_20:
        binaryData = v0.encode(binaryData);
    label_3:
        return binaryData;
    }

    public static byte[] encodeBase64Chunked(byte[] binaryData) {
        return Base64.encodeBase64(binaryData, true);
    }

    public static String encodeBase64String(byte[] binaryData) {
        return StringUtils.newStringUtf8(Base64.encodeBase64(binaryData, true));
    }

    public static byte[] encodeBase64URLSafe(byte[] binaryData) {
        return Base64.encodeBase64(binaryData, false, true);
    }

    public static String encodeBase64URLSafeString(byte[] binaryData) {
        return StringUtils.newStringUtf8(Base64.encodeBase64(binaryData, false, true));
    }

    public static byte[] encodeInteger(BigInteger bigInt) {
        if(bigInt == null) {
            throw new NullPointerException("encodeInteger called with null parameter");
        }

        return Base64.encodeBase64(Base64.toIntegerBytes(bigInt), false);
    }

    public String encodeToString(byte[] pArray) {
        return StringUtils.newStringUtf8(this.encode(pArray));
    }

    private static long getEncodeLength(byte[] pArray, int chunkSize, byte[] chunkSeparator) {
        int v2;
        long v9 = 4;
        long v7 = 0;
        chunkSize = chunkSize / 4 * 4;
        long v0 = ((long)(pArray.length * 4 / 3));
        long v3 = v0 % v9;
        if(v3 != v7) {
            v0 += v9 - v3;
        }

        if(chunkSize > 0) {
            if(v0 % (((long)chunkSize)) == v7) {
                v2 = 1;
            }
            else {
                v2 = 0;
            }

            v0 += v0 / (((long)chunkSize)) * (((long)chunkSeparator.length));
            if(v2 != 0) {
                goto label_27;
            }

            v0 += ((long)chunkSeparator.length);
        }

    label_27:
        return v0;
    }

    boolean hasData() {
        boolean v0;
        if(this.buffer != null) {
            v0 = true;
        }
        else {
            v0 = false;
        }

        return v0;
    }

    public static boolean isArrayByteBase64(byte[] arrayOctet) {
        boolean v1;
        int v0;
        for(v0 = 0; v0 < arrayOctet.length; ++v0) {
            if(!Base64.isBase64(arrayOctet[v0]) && !Base64.isWhiteSpace(arrayOctet[v0])) {
                v1 = false;
                goto label_4;
            }
        }

        v1 = true;
    label_4:
        return v1;
    }

    public static boolean isBase64(byte octet) {
        boolean v0;
        if(octet != 61) {
            if(octet >= 0 && octet < Base64.DECODE_TABLE.length && Base64.DECODE_TABLE[octet] != -1) {
                goto label_12;
            }

            v0 = false;
        }
        else {
        label_12:
            v0 = true;
        }

        return v0;
    }

    public boolean isUrlSafe() {
        boolean v0;
        if(this.encodeTable == Base64.URL_SAFE_ENCODE_TABLE) {
            v0 = true;
        }
        else {
            v0 = false;
        }

        return v0;
    }

    private static boolean isWhiteSpace(byte byteToCheck) {
        boolean v0;
        switch(byteToCheck) {
            case 9: 
            case 10: 
            case 13: 
            case 32: {
                v0 = true;
                break;
            }
            default: {
                v0 = false;
                break;
            }
        }

        return v0;
    }

    int readResults(byte[] b, int bPos, int bAvail) {
        byte[] v3 = null;
        if(this.buffer == null) {
            goto label_20;
        }

        int v0 = Math.min(this.avail(), bAvail);
        if(this.buffer == b) {
            goto label_18;
        }

        System.arraycopy(this.buffer, this.readPos, b, bPos, v0);
        this.readPos += v0;
        if(this.readPos < this.pos) {
            goto label_17;
        }

        this.buffer = v3;
        goto label_17;
    label_18:
        this.buffer = v3;
        goto label_17;
    label_20:
        if(!this.eof) {
            goto label_25;
        }

        int v1 = -1;
        goto label_23;
    label_25:
        v1 = 0;
    label_23:
        v0 = v1;
    label_17:
        return v0;
    }

    private void reset() {
        this.buffer = null;
        this.pos = 0;
        this.readPos = 0;
        this.currentLinePos = 0;
        this.modulus = 0;
        this.eof = false;
    }

    private void resizeBuffer() {
        if(this.buffer != null) {
            goto label_9;
        }

        this.buffer = new byte[8192];
        this.pos = 0;
        this.readPos = 0;
        return;
    label_9:
        byte[] v0 = new byte[this.buffer.length * 2];
        System.arraycopy(this.buffer, 0, v0, 0, this.buffer.length);
        this.buffer = v0;
    }

    void setInitialBuffer(byte[] out, int outPos, int outAvail) {
        if(out == null) {
            return;
        }

        if(out.length != outAvail) {
            return;
        }

        this.buffer = out;
        this.pos = outPos;
        this.readPos = outPos;
    }

    static byte[] toIntegerBytes(BigInteger bigInt) {
        int v1 = bigInt.bitLength() + 7 >> 3 << 3;
        byte[] v0 = bigInt.toByteArray();
        if(bigInt.bitLength() % 8 == 0) {
            goto label_14;
        }

        if(bigInt.bitLength() / 8 + 1 != v1 / 8) {
            goto label_14;
        }

        goto label_13;
    label_14:
        int v5 = 0;
        int v2 = v0.length;
        if(bigInt.bitLength() % 8 != 0) {
            goto label_21;
        }

        v5 = 1;
        --v2;
    label_21:
        byte[] v3 = new byte[v1 / 8];
        System.arraycopy(v0, v5, v3, v1 / 8 - v2, v2);
        v0 = v3;
    label_13:
        return v0;
    }
}

