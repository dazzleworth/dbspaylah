// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.UnsupportedEncodingException;

public class StringUtils {
    public StringUtils() {
        super();
    }

    public static byte[] getBytesIso8859_1(String string) {
        return StringUtils.getBytesUnchecked(string, "ISO-8859-1");
    }

    public static byte[] getBytesUnchecked(String string, String charsetName) {
        if(string != null) {
            goto label_3;
        }

        byte[] v1 = null;
        goto label_2;
        try {
        label_3:
            v1 = string.getBytes(charsetName);
        }
        catch(UnsupportedEncodingException v0) {
            throw StringUtils.newIllegalStateException(charsetName, v0);
        }

    label_2:
        return v1;
    }

    public static byte[] getBytesUsAscii(String string) {
        return StringUtils.getBytesUnchecked(string, "US-ASCII");
    }

    public static byte[] getBytesUtf16(String string) {
        return StringUtils.getBytesUnchecked(string, "UTF-16");
    }

    public static byte[] getBytesUtf16Be(String string) {
        return StringUtils.getBytesUnchecked(string, "UTF-16BE");
    }

    public static byte[] getBytesUtf16Le(String string) {
        return StringUtils.getBytesUnchecked(string, "UTF-16LE");
    }

    public static byte[] getBytesUtf8(String string) {
        return StringUtils.getBytesUnchecked(string, "UTF-8");
    }

    private static IllegalStateException newIllegalStateException(String charsetName, UnsupportedEncodingException e) {
        return new IllegalStateException(String.valueOf(charsetName) + ": " + e);
    }

    public static String newString(byte[] bytes, String charsetName) {
        String v1;
        if(bytes == null) {
            v1 = null;
        }
        else {
            try {
                v1 = new String(bytes, charsetName);
            }
            catch(UnsupportedEncodingException v0) {
                throw StringUtils.newIllegalStateException(charsetName, v0);
            }
        }

        return v1;
    }

    public static String newStringIso8859_1(byte[] bytes) {
        return StringUtils.newString(bytes, "ISO-8859-1");
    }

    public static String newStringUsAscii(byte[] bytes) {
        return StringUtils.newString(bytes, "US-ASCII");
    }

    public static String newStringUtf16(byte[] bytes) {
        return StringUtils.newString(bytes, "UTF-16");
    }

    public static String newStringUtf16Be(byte[] bytes) {
        return StringUtils.newString(bytes, "UTF-16BE");
    }

    public static String newStringUtf16Le(byte[] bytes) {
        return StringUtils.newString(bytes, "UTF-16LE");
    }

    public static String newStringUtf8(byte[] bytes) {
        return StringUtils.newString(bytes, "UTF-8");
    }
}

