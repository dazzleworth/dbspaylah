// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract$CommonDataKinds$Phone;
import android.provider.ContactsContract$Contacts;
import android.provider.ContactsContract$Data;
import android.provider.ContactsContract$PhoneLookup;
import android.util.Base64;
import android.util.Log;
import android.view.ViewConfiguration;
import com.konylabs.android.KonyMain;
import java.util.Hashtable;
import java.util.Vector;

public class FilteredContact {
    public FilteredContact() {
        super();
    }

    public Vector getContactNameFromNumber(String searchString) {
        String v9;
        Cursor v11;
        Vector v8 = new Vector();
        ContentResolver v2 = KonyMain.getActivityContext().getContentResolver();
        if(!searchString.matches("^\\(?\\+?[\\d\\(\\-\\s\\)]+$")) {
            goto label_54;
        }

        Log.e("MyLog", "########### in the search by number if ########## " + searchString);
        String[] v4 = new String[]{"_id", "display_name", "photo_id", "number"};
        Uri v3 = Uri.withAppendedPath(ContactsContract$PhoneLookup.CONTENT_FILTER_URI, Uri.encode(searchString));
        String v5 = null;
        String[] v6 = null;
        String v7 = null;
        try {
            v11 = v2.query(v3, v4, v5, v6, v7);
        }
        catch(Exception v13) {
            Log.e("MyLog", "########### exception occured while quering contacts");
            v13.printStackTrace();
        }

        Log.e("MyLog", "########### after fetching all contact in if ########## ");
        if(v11 == null) {
            goto label_54;
        }

        if(v11.getCount() <= 0) {
            goto label_54;
        }

        Log.e("MyLog", "########### In if cur==" + v11);
        while(v11.moveToNext()) {
            Log.e("MyLog", "########### In while == " + v11);
            Log.e("MyLog", "########### id == " + v11.getString(v11.getColumnIndex("_id")));
            String v15 = v11.getString(v11.getColumnIndex("display_name"));
            Log.e("MyLog", "########### name == " + v15);
            try {
                Log.e("MyLog", "########### colIndex of PHOTO_ID from contacts==" + v11.getColumnIndex("photo_id"));
                int v17 = v11.getInt(v11.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### uri == " + v17);
                v9 = this.queryContactImage(v17);
                Log.e("MyLog", "########### base64 == " + v9);
            }
            catch(Exception v13) {
                Log.e("MyLog", "########### == " + v13.getMessage());
            }

            Hashtable v12 = new Hashtable();
            String v16 = v11.getString(v11.getColumnIndex("number"));
            Log.e("MyLog", "########### Phone number == " + v16);
            try {
                v12.put("displayName", v15);
                v12.put("phone", v16);
                if(v9 != null) {
                    v12.put("base64", v9);
                }

                Log.e("MyLog", "########### currContact==" + v12);
                Log.e("MyLog", "########### adding contact to allcontact==");
                v8.addElement(v12);
                Log.e("MyLog", "########### after adding contact " + v8);
                continue;
            }
            catch(Exception v13) {
                Log.e("MyLog", "########### == " + v13.getMessage());
                continue;
            }
        }

        v11.close();
    label_54:
        Log.e("MyLog", "########### allContacts==" + v8);
        return v8;
    }

    public boolean hasSoftKey() {
        return ViewConfiguration.get(KonyMain.getActivityContext()).hasPermanentMenuKey();
    }

    private String queryContactImage(int imageDataRow) {
        Cursor v6;
        String v10 = null;
        KonyMain v7 = KonyMain.getActivityContext();
        try {
            v6 = ((Context)v7).getContentResolver().query(ContactsContract$Data.CONTENT_URI, new String[]{"data15"}, "_id=?", new String[]{Integer.toString(imageDataRow)}, null);
        }
        catch(Exception v8) {
            Log.e("MyLog", "########### Exception occured while getting image for contact");
            v8.printStackTrace();
        }

        byte[] v9 = ((byte[])v10);
        if(v6 == null) {
            goto label_25;
        }

        if(!v6.moveToFirst()) {
            goto label_24;
        }

        v9 = v6.getBlob(0);
    label_24:
        v6.close();
    label_25:
        if(v9 == null) {
            goto label_27;
        }

        v10 = Base64.encodeToString(v9, 0);
    label_27:
        return v10;
    }

    public Vector readFilteredContacts(String searchString) {
        Cursor v23;
        String v24;
        Hashtable v18;
        String v15;
        int v25;
        String v22;
        String v21;
        Cursor v17;
        Vector v14 = new Vector();
        KonyMain v16 = KonyMain.getActivityContext();
        ContentResolver v2 = ((Context)v16).getContentResolver();
        ViewConfiguration.get(((Context)v16)).hasPermanentMenuKey();
        if(!searchString.matches("^\\(?\\+?[\\d\\(\\-\\s\\)]+$")) {
            goto label_206;
        }

        Log.e("MyLog", "########### in the search by number if");
        Uri v3 = ContactsContract$CommonDataKinds$Phone.CONTENT_URI;
        String[] v4 = new String[]{"_id", "display_name", "photo_id", "data1"};
        String v5 = "data1 like \'%" + searchString + "%\'";
        String[] v6 = null;
        String v7 = "display_name COLLATE LOCALIZED ASC";
        try {
            v17 = v2.query(v3, v4, v5, v6, v7);
        }
        catch(Exception v19) {
            Log.e("MyLog", "########### Exception occured while quering contacts");
            v19.printStackTrace();
        }

        Log.e("MyLog", "########### after fetching all contact in if");
        if(v17 == null) {
            goto label_57;
        }

        if(v17.getCount() <= 0) {
            goto label_57;
        }

        Log.e("MyLog", "########### In if cur==" + v17);
        while(v17.moveToNext()) {
            Log.e("MyLog", "########### In while==" + v17);
            v21 = v17.getString(v17.getColumnIndex("_id"));
            Log.e("MyLog", "########### id==" + v21);
            v22 = v17.getString(v17.getColumnIndex("display_name"));
            Log.e("MyLog", "########### name==" + v22);
            try {
                Log.e("MyLog", "########### colIndex name==" + v17.getColumnIndex("display_name"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID==" + v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID from contacts==" + v17.getColumnIndex("photo_id"));
                v25 = v17.getInt(v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### uri==" + v25);
                v15 = this.queryContactImage(v25);
                Log.e("MyLog", "########### base64==" + v15);
            }
            catch(Exception v19) {
                v19.printStackTrace();
            }

            Log.e("MyLog", "########### in second if==");
            System.out.println("name : " + v22 + ", ID : " + v21);
            v18 = new Hashtable();
            v24 = v17.getString(v17.getColumnIndex("data1"));
            try {
                v18.put("displayName", v22);
                v18.put("phone", v24);
                if(v15 != null) {
                    v18.put("base64", v15);
                }

                Log.e("MyLog", "########### currContact==" + v18);
                Log.e("MyLog", "########### adding contact to allcontact==");
                v14.addElement(v18);
                Log.e("MyLog", "########### after adding contact " + v14);
                continue;
            }
            catch(Exception v19) {
                v19.printStackTrace();
                continue;
            }
        }

        v17.close();
        goto label_57;
    label_206:
        v3 = ContactsContract$Contacts.CONTENT_URI;
        v4 = new String[]{"_id", "display_name", "photo_id", "has_phone_number"};
        v5 = "display_name like \'%" + searchString + "%\'";
        v6 = null;
        v7 = "display_name COLLATE LOCALIZED ASC";
        try {
            v17 = v2.query(v3, v4, v5, v6, v7);
        }
        catch(Exception v19) {
            Log.e("MyLog", "########### Exception occured while quering contacts");
            v19.printStackTrace();
        }

        Log.e("MyLog", "########### prahtam" + v2);
        Log.e("MyLog", "########### after fetching contact");
        if(v17 == null) {
            goto label_57;
        }

        if(v17.getCount() <= 0) {
            goto label_57;
        }

        Log.e("MyLog", "########### In if cur==" + v17);
        do {
        label_250:
            if(v17.moveToNext()) {
                goto label_260;
            }

            goto label_252;
        label_260:
            Log.e("MyLog", "########### In while==" + v17);
            v21 = v17.getString(v17.getColumnIndex("_id"));
            Log.e("MyLog", "########### id==" + v21);
            v22 = v17.getString(v17.getColumnIndex("display_name"));
            Log.e("MyLog", "########### name==" + v22);
            try {
                Log.e("MyLog", "########### colIndex name==" + v17.getColumnIndex("display_name"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID==" + v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### colIndex of PHOTO_ID from contacts==" + v17.getColumnIndex("photo_id"));
                v25 = v17.getInt(v17.getColumnIndex("photo_id"));
                Log.e("MyLog", "########### uri==" + v25);
                v15 = this.queryContactImage(v25);
                Log.e("MyLog", "########### base64==" + v15);
            }
            catch(Exception v19) {
                v19.printStackTrace();
            }
        }
        while(Integer.parseInt(v17.getString(v17.getColumnIndex("has_phone_number"))) <= 0);

        Log.e("MyLog", "########### in second if==");
        System.out.println("name : " + v22 + ", ID : " + v21);
        try {
            v23 = v2.query(ContactsContract$CommonDataKinds$Phone.CONTENT_URI, null, "contact_id = ?", new String[]{v21}, null);
        }
        catch(Exception v19) {
            Log.e("MyLog", "########### exception occured while fetching the contact number for this contact ID " + v21 + "name = " + v22);
            v19.printStackTrace();
        }

        System.out.println("After Fetching all phone numbers" + v23);
        if(v23 == null) {
            goto label_381;
        }

        goto label_379;
    label_252:
        v17.close();
        goto label_57;
    label_379:
        while(v23.moveToNext()) {
            v18 = new Hashtable();
            v24 = v23.getString(v23.getColumnIndex("data1"));
            try {
                v18.put("displayName", v22);
                v18.put("phone", v24);
                if(v15 != null) {
                    v18.put("base64", v15);
                }

                Log.e("MyLog", "########### currContact==" + v18);
                Log.e("MyLog", "########### adding contact to allcontact==");
                v14.addElement(v18);
                Log.e("MyLog", "########### after adding contact " + v14);
                continue;
            }
            catch(Exception v19) {
                v19.printStackTrace();
                continue;
            }
        }

    label_381:
        v23.close();
        goto label_250;
    label_57:
        Log.e("MyLog", "########### allContacts==" + v14);
        return v14;
    }
}

