// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Iterator;
import org.apache.http.conn.util.InetAddressUtils;

public class Utils {
    public Utils() {
        super();
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder v2 = new StringBuilder();
        int v0 = 0;
    label_2:
        if(v0 < bytes.length) {
            goto label_6;
        }

        goto label_4;
    label_6:
        int v1 = bytes[v0] & 255;
        if(v1 >= 16) {
            goto label_12;
        }

        v2.append("0");
    label_12:
        v2.append(Integer.toHexString(v1).toUpperCase());
        ++v0;
        goto label_2;
    label_4:
        return v2.toString();
    }

    public static String getIPAddress(boolean useIPv4) {
        String v6;
        try {
            Iterator v7_1 = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
        label_3:
            if(v7_1.hasNext()) {
                goto label_7;
            }

            goto label_5;
        label_7:
            Iterator v8 = Collections.list(v7_1.next().getInetAddresses()).iterator();
            do {
            label_11:
                if(!v8.hasNext()) {
                    goto label_3;
                }

                Object v0 = v8.next();
                if(((InetAddress)v0).isLoopbackAddress()) {
                    goto label_11;
                }

                v6 = ((InetAddress)v0).getHostAddress().toUpperCase();
                boolean v5 = InetAddressUtils.isIPv4Address(v6);
                if(!useIPv4) {
                    continue;
                }

                if(!v5) {
                    goto label_11;
                }

                goto label_6;
            }
            while(v5);

            int v2 = v6.indexOf(37);
            if(v2 < 0) {
                goto label_6;
            }

            v6 = v6.substring(0, v2);
            goto label_6;
        }
        catch(Exception v7) {
        }

    label_5:
        v6 = "";
    label_6:
        return v6;
    }

    public static String getMACAddress(String interfaceName) {
        byte[] v4;
        Object v3;
        try {
            Iterator v5_1 = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
            do {
                if(v5_1.hasNext()) {
                    goto label_7;
                }

                goto label_5;
            label_7:
                v3 = v5_1.next();
                if(interfaceName == null) {
                    break;
                }
            }
            while(!((NetworkInterface)v3).getName().equalsIgnoreCase(interfaceName));

            v4 = ((NetworkInterface)v3).getHardwareAddress();
            if(v4 != null) {
                goto label_16;
            }
        }
        catch(Exception v5) {
            goto label_39;
        }

        String v5_2 = "";
        goto label_6;
        try {
        label_16:
            StringBuilder v0 = new StringBuilder();
            int v1;
            for(v1 = 0; v1 < v4.length; ++v1) {
                v0.append(String.format("%02X:", Byte.valueOf(v4[v1])));
            }

            if(v0.length() > 0) {
                v0.deleteCharAt(v0.length() - 1);
            }

            v5_2 = v0.toString();
            goto label_6;
        }
        catch(Exception v5) {
        label_39:
        }

    label_5:
        v5_2 = "";
    label_6:
        return v5_2;
    }

    public static byte[] getUTF8Bytes(String str) {
        byte[] v1;
        try {
            v1 = str.getBytes("UTF-8");
        }
        catch(Exception v0) {
            v1 = null;
        }

        return v1;
    }

    public static String loadFileAsString(String filename) throws IOException {
        String v7_1;
        int v6;
        int v3;
        int v5;
        byte[] v2;
        ByteArrayOutputStream v1;
        BufferedInputStream v4 = new BufferedInputStream(new FileInputStream(filename), 1024);
        try {
            v1 = new ByteArrayOutputStream(1024);
            v2 = new byte[1024];
            v5 = 0;
            v3 = 0;
            while(true) {
            label_10:
                v6 = v4.read(v2);
                if(v6 != -1) {
                    break;
                }

                goto label_13;
            }
        }
        catch(Throwable v7) {
            goto label_42;
        }

        if(v3 == 0) {
            try {
                if(v2[0] != -17 || v2[1] != -69 || v2[2] != -65) {
                label_38:
                    v1.write(v2, 0, v6);
                }
                else {
                    v5 = 1;
                    v1.write(v2, 3, v6 - 3);
                }

            label_36:
                v3 += v6;
                goto label_10;
            label_13:
                if(v5 != 0) {
                    v7_1 = new String(v1.toByteArray(), "UTF-8");
                }
                else {
                    v7_1 = new String(v1.toByteArray());
                }

                goto label_17;
            }
            catch(Throwable v7) {
                goto label_42;
            }
        }
        else {
            goto label_38;
        }

        goto label_36;
        try {
        label_17:
            v4.close();
        }
        catch(Exception v8) {
        }

        return v7_1;
        try {
        label_42:
            v4.close();
        }
        catch(Exception v8) {
        }

        throw v7;
    }
}

