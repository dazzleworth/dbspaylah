// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

public class DecoderException extends Exception {
    private static final long serialVersionUID = 1;

    public DecoderException() {
        super();
    }

    public DecoderException(String message) {
        super(message);
    }

    public DecoderException(String message, Throwable cause) {
        super(message, cause);
    }

    public DecoderException(Throwable cause) {
        super(cause);
    }
}

