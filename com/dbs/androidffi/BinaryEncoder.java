// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

public abstract interface BinaryEncoder extends Encoder {
    public abstract byte[] encode(byte[] arg0) throws EncoderException;
}

