// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import com.konylabs.android.KonyMain;
import vkey.android.vguard.VGuard;
import vkey.android.vguard.VGuardManager;
import vkey.android.vguard.securekeypad.VKeySecureKeypad;

public class VKey {
    private static VGuard vGuardMgr;

    public VKey() {
        super();
    }

    public static void setVKey() {
        VKey.vGuardMgr = VGuardManager.sharedVGuard(KonyMain.getActivityContext());
        VKeySecureKeypad.NUMPAD_CLOSE_CHAR = "DONE";
        VKeySecureKeypad.NUMPAD_DELETE_CHAR = "Delete";
        VKeySecureKeypad.KEYBOARD_CLOSE_CHAR = "Go";
        VKeySecureKeypad.KEYBOARD_DELETE_CHAR = "Del";
    }
}

