// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.security.Key;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES128CryptoGram {
    private static SecretKey AESKey;
    private static String ALGO;
    private static String PADDING;

    static  {
        AES128CryptoGram.ALGO = "AES";
        AES128CryptoGram.PADDING = "AES/CBC/PKCS5Padding";
    }

    public AES128CryptoGram() {
        super();
    }

    public Vector decrypt(String randByteStr, Vector arg23) {
        Vector v8;
        Iterator v13;
        Cipher v7;
        Vector v9;
        try {
            v9 = new Vector();
        }
        catch(Exception v10) {
            goto label_53;
        }

        try {
            v7 = Cipher.getInstance(AES128CryptoGram.PADDING);
            Key v16 = this.generateAESKey(Base64.decodeBase64(randByteStr));
            v7.init(2, v16, new IvParameterSpec(v16.getEncoded()));
            v13 = arg23.iterator();
            goto label_16;
        }
        catch(Exception v10) {
            goto label_45;
        }

    label_53:
        goto label_45;
        try {
        label_16:
            while(v13.hasNext()) {
                new ByteArrayOutputStream();
                ByteArrayOutputStream v15 = new ByteArrayOutputStream();
                CipherInputStream v4 = new CipherInputStream(new ByteArrayInputStream(Base64.decodeBase64(v13.next())), v7);
                byte[] v2 = new byte[1024];
                while(true) {
                    int v3 = v4.read(v2);
                    if(v3 >= 0) {
                        goto label_49;
                    }

                    break;
                label_49:
                    v15.write(v2, 0, v3);
                }

                String v6 = new String(v15.toByteArray());
                v9.add(v6);
                System.out.println("decryptedString: " + v6);
            }

            v8 = v9;
            goto label_19;
        }
        catch(Exception v10) {
        }

    label_45:
        v8 = null;
        v10.printStackTrace();
    label_19:
        return v8;
    }

    public Vector encrypt(String randByteStr, Vector arg19) {
        Iterator v9;
        Cipher v5;
        Vector v8;
        Vector v7 = null;
        try {
            v8 = new Vector();
        }
        catch(Exception v4) {
            goto label_42;
        }

        try {
            byte[] v13 = Base64.decodeBase64(randByteStr);
            v5 = Cipher.getInstance(AES128CryptoGram.PADDING);
            Key v12 = this.generateAESKey(v13);
            v5.init(1, v12, new IvParameterSpec(v12.getEncoded()));
            v9 = arg19.iterator();
            goto label_12;
        }
        catch(Exception v4) {
            goto label_37;
        }

    label_42:
        goto label_38;
        try {
        label_12:
            while(v9.hasNext()) {
                ByteArrayOutputStream v11 = new ByteArrayOutputStream();
                CipherOutputStream v2 = new CipherOutputStream(((OutputStream)v11), v5);
                v2.write(v9.next().getBytes());
                v2.flush();
                v2.close();
                byte[] v6 = v11.toByteArray();
                System.out.println("Encrypted Result: " + new String(v6));
                v8.add(Base64.encodeBase64String(v6));
            }

            v7 = v8;
        }
        catch(Exception v4) {
        label_37:
            v7 = v8;
            goto label_38;
        }

    label_15:
        return v7;
    label_38:
        v4.printStackTrace();
        goto label_15;
    }

    public Key generateAESKey(byte[] randBytes) {
        SecretKeySpec v0 = new SecretKeySpec(randBytes, AES128CryptoGram.ALGO);
        System.out.println("Random Key String value is :  " + new String(Base64.encodeBase64(((Key)v0).getEncoded())));
        return ((Key)v0);
    }

    public String generateRandomKey() {
        byte[] v1 = new byte[16];
        new Random().nextBytes(v1);
        String v2 = new String(Base64.encodeBase64(v1));
        System.out.println("Random String value is :  " + v2);
        return v2;
    }

    public SecretKey keyGenerator() {
        SecretKey v0;
        try {
            KeyGenerator v3 = KeyGenerator.getInstance(AES128CryptoGram.ALGO);
            v3.init(128);
            v0 = v3.generateKey();
            new String(Base64.encodeBase64(v0.getEncoded()));
        }
        catch(Exception v2) {
            v2.printStackTrace();
            v0 = null;
        }

        return v0;
    }

    public static void main(String[] args) {
    }
}

