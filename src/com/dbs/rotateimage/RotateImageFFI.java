// Decompiled by JEB v1.4.201311050

package com.dbs.rotateimage;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.util.Locale;

public class RotateImageFFI {
    public static String bitmapTobase64(Bitmap b) {
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In the convert to base64 function");
        Bitmap image = b;
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Bitmap in convert to base64");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(CompressFormat.PNG, 100, baos);
        String rbase64 = Base64.encodeToString(baos.toByteArray(), 0);
        Log.e("rotate", new StringBuilder("Rotated base64").append(rbase64).toString());
        image.recycle();
        return rbase64;
    }

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        Log.e("Rotate", new StringBuilder("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ original height ==").append(height).toString());
        Log.e("Rotate", new StringBuilder("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ original height ==").append(width).toString());
        if (height <= reqHeight && width <= reqWidth) {
            return inSampleSize;
        }
        int halfHeight = height / 5;
        int halfWidth = width / 5;
        while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
            inSampleSize *= 2;
        }
        return inSampleSize;
    }

    public static Bitmap rotate(Bitmap b) {
        if (b == null) {
            return b;
        }
        Matrix m = new Matrix();
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In the rotate Image function");
        m.setRotate((float) 90, ((float) b.getWidth()) / 2.0f, ((float) b.getHeight()) / 2.0f);
        try {
            Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Creating  bitmap in rotate function");
            if (b == b2) {
                return b;
            }
            b.recycle();
            return b2;
        } catch (OutOfMemoryError e) {
            throw e;
        }
    }

    public static String rotateFFI(String base64) {
        String manufacturer = Build.MANUFACTURER;
        if (!manufacturer.toLowerCase(Locale.getDefault()).contains("samsung") && !manufacturer.toLowerCase().contains("sony")) {
            return null;
        }
        byte[] decodedString = Base64.decode(base64, 0);
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
        options.inSampleSize = calculateInSampleSize(options, 320, 240);
        options.inJustDecodeBounds = false;
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Creating initial bitmap");
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length, options);
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Calling rotate image function");
        Bitmap rotatedBitmap = rotate(decodedByte);
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ After rotate bitmap function");
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Calling convert to base64 function");
        String r4_String = bitmapTobase64(rotatedBitmap);
        decodedByte.recycle();
        rotatedBitmap.recycle();
        return r4_String;
    }
}

