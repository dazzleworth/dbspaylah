// Decompiled by JEB v1.4.201311050

package com.dbs.ffi;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.util.Base64;
import android.util.Log;

import com.kony.crypto.util.CryptoConstants;
import com.konylabs.android.KonyMain;

import java.util.Hashtable;
import java.util.Vector;

public class FilteredContact {
    public FilteredContact() {
        super();
    }

    private String queryContactImage(int imageDataRow) {
        String v5 = null;
        Cursor v6 = KonyMain.getActivityContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data15"}, "_id=?", new String[]{Integer.toString(imageDataRow)}, v5);
        byte[] v8 = null;
        if(v6 != null) {
            if(v6.moveToFirst()) {
                v8 = v6.getBlob(0);
            }

            v6.close();
        }

        if(v8 != null) {
            v5 = Base64.encodeToString(v8, 0);
        }

        return v5;
    }

    public Vector<Hashtable<String, String>> readFilteredContacts(String searchString) {
        String base64 = CryptoConstants.EMPTY;
        Vector<Hashtable<String, String>> allContacts = new Vector<Hashtable<String, String>>();
        ContentResolver cr = KonyMain.getActivityContext().getContentResolver();
        Uri uri;
        String[] projection;
        Cursor cur;
        String id;
        String name;
        int photoId;
        Hashtable<String, String> currContact;
        String phone;
        if (searchString.matches("-?\\d+(\\.\\d+)?")) {
            Log.e("MyLog", "########### in the search by number if");
            uri = Phone.CONTENT_URI;
            projection = new String[4];
            projection[0] = "_id";
            projection[1] = "display_name";
            projection[2] = "photo_id";
            projection[3] = "data1";
            cur = cr.query(uri, projection, new StringBuilder("data1 like '%").append(searchString).append("%'").toString(), (String[]) null, "display_name COLLATE LOCALIZED ASC");
            Log.e("MyLog", "########### after fetching all contact in if");
            if (cur.getCount() > 0) {
                Log.e("MyLog", new StringBuilder("########### In if cur==").append(cur).toString());
                while (cur.moveToNext()) {
                    Log.e("MyLog", new StringBuilder("########### In while==").append(cur).toString());
                    id = cur.getString(cur.getColumnIndex("_id"));
                    Log.e("MyLog", new StringBuilder("########### id==").append(id).toString());
                    name = cur.getString(cur.getColumnIndex("display_name"));
                    Log.e("MyLog", new StringBuilder("########### name==").append(name).toString());
                    try {
                        Log.e("MyLog", new StringBuilder("########### colIndex name==").append(cur.getColumnIndex("display_name")).toString());
                        Log.e("MyLog", new StringBuilder("########### colIndex of PHOTO_ID==").append(cur.getColumnIndex("photo_id")).toString());
                        Log.e("MyLog", new StringBuilder("########### colIndex of PHOTO_ID from contacts==").append(cur.getColumnIndex("photo_id")).toString());
                        photoId = cur.getInt(cur.getColumnIndex("photo_id"));
                        Log.e("MyLog", new StringBuilder("########### uri==").append(photoId).toString());
                        base64 = queryContactImage(photoId);
                        Log.e("MyLog", new StringBuilder("########### base64==").append(base64).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("MyLog", "########### in second if==");
                    System.out.println(new StringBuilder("name : ").append(name).append(", ID : ").append(id).toString());
                    currContact = new Hashtable<String,String>();
                    phone = cur.getString(cur.getColumnIndex("data1"));
                    try {
                        currContact.put("displayName", name);
                        currContact.put("phone", phone);
                        if (base64 != null) {
                            currContact.put("base64", base64);
                        }
                        Log.e("MyLog", new StringBuilder("########### currContact==").append(currContact).toString());
                        Log.e("MyLog", "########### adding contact to allcontact==");
                        allContacts.addElement(currContact);
                        Log.e("MyLog", new StringBuilder("########### after adding contact ").append(allContacts).toString());
                    } catch (Exception e_2) {
                        e_2.printStackTrace();
                    }
                }
            }
        } else {
            uri = Contacts.CONTENT_URI;
            projection = new String[4];
            projection[0] = "_id";
            projection[1] = "display_name";
            projection[2] = "photo_id";
            projection[3] = "has_phone_number";
            cur = cr.query(uri, projection, new StringBuilder("display_name like '%").append(searchString).append("%'").toString(), null, "display_name COLLATE LOCALIZED ASC");
            Log.e("MyLog", new StringBuilder("########### prahtam").append(cr).toString());
            Log.e("MyLog", "########### after fetching contact");
            if (cur.getCount() > 0) {
                Log.e("MyLog", new StringBuilder("########### In if cur==").append(cur).toString());
                while (cur.moveToNext()) {
                    Log.e("MyLog", new StringBuilder("########### In while==").append(cur).toString());
                    id = cur.getString(cur.getColumnIndex("_id"));
                    Log.e("MyLog", new StringBuilder("########### id==").append(id).toString());
                    name = cur.getString(cur.getColumnIndex("display_name"));
                    Log.e("MyLog", new StringBuilder("########### name==").append(name).toString());
                    try {
                        Log.e("MyLog", new StringBuilder("########### colIndex name==").append(cur.getColumnIndex("display_name")).toString());
                        Log.e("MyLog", new StringBuilder("########### colIndex of PHOTO_ID==").append(cur.getColumnIndex("photo_id")).toString());
                        Log.e("MyLog", new StringBuilder("########### colIndex of PHOTO_ID from contacts==").append(cur.getColumnIndex("photo_id")).toString());
                        photoId = cur.getInt(cur.getColumnIndex("photo_id"));
                        Log.e("MyLog", new StringBuilder("########### uri==").append(photoId).toString());
                        base64 = queryContactImage(photoId);
                        Log.e("MyLog", new StringBuilder("########### base64==").append(base64).toString());
                    } catch (Exception e_3) {
                        e_3.printStackTrace();
                    }
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex("has_phone_number"))) > 0) {
                        Log.e("MyLog", "########### in second if==");
                        System.out.println(new StringBuilder("name : ").append(name).append(", ID : ").append(id).toString());
                        Uri r9_Uri = Phone.CONTENT_URI;
                        String[] r12_StringA = new String[1];
                        r12_StringA[0] = id;
                        Cursor pCur = cr.query(r9_Uri, null, "contact_id = ?", r12_StringA, null);
                        System.out.println(new StringBuilder("After Fetching all phone numbers").append(pCur).toString());
                        while (pCur.moveToNext()) {
                            currContact = new Hashtable<String,String>();
                            phone = pCur.getString(pCur.getColumnIndex("data1"));
                            try {
                                currContact.put("displayName", name);
                                currContact.put("phone", phone);
                                if (base64 != null) {
                                    currContact.put("base64", base64);
                                }
                                Log.e("MyLog", new StringBuilder("########### currContact==").append(currContact).toString());
                                Log.e("MyLog", "########### adding contact to allcontact==");
                                allContacts.addElement(currContact);
                                Log.e("MyLog", new StringBuilder("########### after adding contact ").append(allContacts).toString());
                            } catch (Exception e_4) {
                                e_4.printStackTrace();
                            }
                        }
                        pCur.close();
                    }
                }
            }
        }
        Log.e("MyLog", new StringBuilder("########### allContacts==").append(allContacts).toString());
        return allContacts;
    }

}

