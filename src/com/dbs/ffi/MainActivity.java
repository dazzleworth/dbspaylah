// Decompiled by JEB v1.4.201311050

package com.dbs.ffi;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import android.util.Base64;
import android.util.Log;

import com.kony.crypto.util.CryptoConstants;
import com.konylabs.android.KonyMain;
import java.util.Hashtable;
import java.util.Vector;

public class MainActivity extends Activity {
    public MainActivity() {
        super();
    }

    public static void main(String[] args) {
    }

    private String queryContactImage(int imageDataRow) {
        String v5 = null;
        Cursor v6 = KonyMain.getActivityContext().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data15"}, "_id=?", new String[]{Integer.toString(imageDataRow)}, v5);
        byte[] v8 = null;
        if(v6 != null) {
            if(v6.moveToFirst()) {
                v8 = v6.getBlob(0);
            }

            v6.close();
        }

        if(v8 != null) {
            v5 = Base64.encodeToString(v8, 0);
        }

        return v5;
    }

    public Vector<Hashtable<String, String>> readContacts() {
        Log.e("MyLog", "###########In readContacts");
        Context context = KonyMain.getActivityContext();
        Log.e("MyLog", new StringBuilder("###########new context").append(context).toString());
        ContentResolver cr = context.getContentResolver();
        Log.e("MyLog", new StringBuilder("########### prahtam").append(cr).toString());
        Cursor cur = cr.query(Contacts.CONTENT_URI, null, null, null, null);
        Log.e("MyLog", "########### after fetching contact");
        String base64 = CryptoConstants.EMPTY;
        Vector<Hashtable<String, String>> allContacts = new Vector<Hashtable<String, String>>();
        if (cur.getCount() > 0) {
            Log.e("MyLog", new StringBuilder("########### In if cur==").append(cur).toString());
            while (cur.moveToNext()) {
                Log.e("MyLog", new StringBuilder("########### In while==").append(cur).toString());
                String id = cur.getString(cur.getColumnIndex("_id"));
                Log.e("MyLog", new StringBuilder("########### id==").append(id).toString());
                String name = cur.getString(cur.getColumnIndex("display_name"));
                Log.e("MyLog", new StringBuilder("########### name==").append(name).toString());
                try {
                    Log.e("MyLog", new StringBuilder("########### colIndex name==").append(cur.getColumnIndex("display_name")).toString());
                    Log.e("MyLog", new StringBuilder("########### colIndex of PHOTO_ID==").append(cur.getColumnIndex("photo_id")).toString());
                    Log.e("MyLog", new StringBuilder("########### colIndex of PHOTO_ID from contacts==").append(cur.getColumnIndex("photo_id")).toString());
                    int photoId = cur.getInt(cur.getColumnIndex("photo_id"));
                    Log.e("MyLog", new StringBuilder("########### uri==").append(photoId).toString());
                    base64 = queryContactImage(photoId);
                    Log.e("MyLog", new StringBuilder("########### base64==").append(base64).toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Integer.parseInt(cur.getString(cur.getColumnIndex("has_phone_number"))) > 0) {
                    Log.e("MyLog", "########### in second if==");
                    System.out.println(new StringBuilder("name : ").append(name).append(", ID : ").append(id).toString());
                    Uri r3_Uri = Phone.CONTENT_URI;
                    String[] r6_StringA = new String[1];
                    r6_StringA[0] = id;
                    Cursor pCur = cr.query(r3_Uri, null, "contact_id = ?", r6_StringA, null);
                    System.out.println(new StringBuilder("After Fetching all phone numbers").append(pCur).toString());
                    while (pCur.moveToNext()) {
                        Hashtable<String, String> currContact = new Hashtable<String, String>();
                        String phone = pCur.getString(pCur.getColumnIndex("data1"));
                        try {
                            currContact.put("displayName", name);
                            currContact.put("phone", phone);
                            if (base64 != null) {
                                currContact.put("base64", base64);
                            }
                            Log.e("MyLog", new StringBuilder("########### currContact==").append(currContact).toString());
                            Log.e("MyLog", "########### adding contact to allcontact==");
                            allContacts.addElement(currContact);
                            Log.e("MyLog", new StringBuilder("########### after adding contact ").append(allContacts).toString());
                        } catch (Exception e_2) {
                            e_2.printStackTrace();
                        }
                    }
                    pCur.close();
                }
            }
        }
        Log.e("MyLog", new StringBuilder("########### allContacts==").append(allContacts).toString());
        return allContacts;
    }

}

