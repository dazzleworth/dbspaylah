// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class CipherUtils {
    private static byte[] key;

    static  {
        CipherUtils.key = new byte[]{116, 104, 105, 115, 73, 115, 65, 83, 101, 99, 114, 101, 116, 75, 101, 121};
    }

    public CipherUtils() {
        super();
    }

    public static String decrypt(String strToDecrypt) {
        String v1;
        try {
            Cipher v0 = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            v0.init(2, new SecretKeySpec(CipherUtils.key, "AES"));
            v1 = new String(v0.doFinal(Base64.decodeBase64(strToDecrypt)));
        }
        catch(Exception v2) {
            v2.printStackTrace();
            v1 = null;
        }

        return v1;
    }

    public static String encrypt(String strToEncrypt) {
        String v2;
        try {
            Cipher v0 = Cipher.getInstance("AES/ECB/PKCS5Padding");
            v0.init(1, new SecretKeySpec(CipherUtils.key, "AES"));
            v2 = Base64.encodeBase64String(v0.doFinal(strToEncrypt.getBytes()));
        }
        catch(Exception v1) {
            v1.printStackTrace();
            v2 = null;
        }

        return v2;
    }

    public static void main(String[] args) {
        String v1 = CipherUtils.encrypt("I am a good boy".trim());
        System.out.println("String to Encrypt : " + "I am a good boy");
        System.out.println("Encrypted : " + v1);
        String v0 = CipherUtils.decrypt(v1);
        System.out.println("String To Decrypt : " + v1);
        System.out.println("Decrypted : " + v0);
    }
}

