// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;

public class AESClient {
    //private static SecretKey AESKey;
    private static String ALGO;
    private static String PADDING;

    static  {
        AESClient.ALGO = "AES";
        AESClient.PADDING = "AES/CBC/PKCS5Padding";
    }

    public AESClient() {
        super();
    }

    public Hashtable<String,String> aesdecrypt(String randByteStr, Hashtable<String, String> arg21) {
    	Hashtable<String, String> v8;
        Iterator<Entry<String, String>> v13;
        Cipher v7;
        Hashtable<String, String> v9;
        try {
            v9 = new Hashtable<String,String>();
            v7 = Cipher.getInstance(AESClient.PADDING);
            Key v16 = this.generateAESKey(Base64.decodeBase64(randByteStr));
            v7.init(2, v16, new IvParameterSpec(v16.getEncoded()));
            v13 = arg21.entrySet().iterator();
            
            while(v13.hasNext()) {
                new ByteArrayOutputStream();
                ByteArrayOutputStream v15 = new ByteArrayOutputStream();
                Map.Entry<String, String> v5 = (Entry<String, String>) v13.next();
                CipherInputStream v4 = new CipherInputStream(new ByteArrayInputStream(Base64.decodeBase64(((Map.Entry<String, String>)v5).getValue())), v7);
                byte[] v2 = new byte[1024];
                while(true) {
                    int v3 = v4.read(v2);
                    if(v3 < 0) {
                    	v15.write(v2, 0, v3);
                    }

                    break;
                    
                }

                v9.put(v5.getKey(), new String(v15.toByteArray()));
            }

            v8 = v9;
            
        }
        catch(Exception v10) {
        	v8 = null;
            v10.printStackTrace();
        }

        try {
            
        }
        catch(Exception v10) {
        	v8 = null;
            v10.printStackTrace();
        }
        
        return v8;
    }

    public Hashtable<String,String> aesencrypt(String randByteStr, Hashtable<String,String> arg19) {
        Iterator<Entry<String,String>> v10;
        Cipher v6;
        Hashtable<String,String> v9 = null;
        Hashtable<String,String> v8 = null;
        try {
            v9 = new Hashtable<String,String>();
            byte[] v14 = Base64.decodeBase64(randByteStr);
            v6 = Cipher.getInstance(AESClient.PADDING);
            Key v13 = this.generateAESKey(v14);
            v6.init(1, v13, new IvParameterSpec(v13.getEncoded()));
            v10 = arg19.entrySet().iterator();
            while(v10.hasNext()) {
                ByteArrayOutputStream v12 = new ByteArrayOutputStream();
                CipherOutputStream v2 = new CipherOutputStream(((OutputStream)v12), v6);
                Map.Entry<String, String> v3 = (Map.Entry<String, String>) v10.next();
                if((v3.getKey().equals("deviceId")) && (v3.getKey().equals("encryptedAES128Key"))) {
                    v9.put(v3.getKey(), v3.getValue());
                    continue;
                }

                v2.write(v3.getValue().getBytes());
                v2.flush();
                v2.close();
                v9.put(v3.getKey(), Base64.encodeBase64String(v12.toByteArray()));
            }

            v8 = v9;
        }
        catch(Exception v5) {
        	v8 = v9;
        	v5.printStackTrace();
        }
        
        return v8;

    }

    public String decrypt(String randByteStr, String encryptedString) {
        String v1 = null;
        String v4 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        try {
            v1 = Base64.encodeBase64String(this.decryptNew(Base64.decodeBase64(encryptedString), v4));
        }
        catch(Exception v2) {
            v2.printStackTrace();
        }

        return v1;
    }

    public byte[] decryptNew(byte[] cipherText, String password) throws Exception {
        return this.transform(false, cipherText, password);
    }

    public String encrypt(String randByteStr, String stringToEncrypt) {
        String v3 = null;
        String v4 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        try {
            v3 = Base64.encodeBase64String(this.encryptNew(Base64.decodeBase64(stringToEncrypt), v4));
        }
        catch(Exception v1) {
            v1.printStackTrace();
        }

        return v3;
    }

    public byte[] encryptNew(byte[] plainText, String password) throws Exception {
        return this.transform(true, plainText, password);
    }

    public Key generateAESKey(byte[] randBytes) {
        SecretKeySpec v0 = new SecretKeySpec(randBytes, AESClient.ALGO);
        Base64.encodeBase64String(((Key)v0).getEncoded());
        return ((Key)v0);
    }

    public String generateRandomKey() {
        byte[] v1 = new byte[16];
        new Random().nextBytes(v1);
        return Base64.encodeBase64String(v1);
    }

    public static String generateRandomKeyFromSHA(String SHA256Str) {
        byte[] v1 = Base64.decodeBase64(SHA256Str);
        new Random().nextBytes(v1);
        return Base64.encodeBase64String(v1);
    }

    public static void main(String[] args) {
    }

    public static String sha256(String base) {
        try {
            byte[] v2 = MessageDigest.getInstance("SHA-256").digest(base.getBytes("UTF-8"));
            StringBuffer v4 = new StringBuffer();
            int v5;
            for(v5 = 0; v5 < v2.length; ++v5) {
                String v3 = Integer.toHexString(v2[v5] & 255);
                if(v3.length() == 1) {
                    v4.append('0');
                }

                v4.append(v3);
            }

            return v4.toString();
        }
        catch(Exception v1) {
            throw new RuntimeException(((Throwable)v1));
        }
    }

    private byte[] transform(boolean encrypt, byte[] inputBytes, String password) throws Exception {
        int v11;
        byte[] v9 = null;
        try {
            MessageDigest v6 = MessageDigest.getInstance("MD5");
            v6.update(password.getBytes("UTF-8"));
            v9 = v6.digest();
        }
        catch(NoSuchAlgorithmException v7) {
            v7.printStackTrace();
        }

        PaddedBufferedBlockCipher v0 = new PaddedBufferedBlockCipher(new CBCBlockCipher(new AESFastEngine()));
        ((BufferedBlockCipher)v0).init(encrypt, new KeyParameter(v9));
        ByteArrayInputStream v8 = new ByteArrayInputStream(inputBytes);
        ByteArrayOutputStream v10 = new ByteArrayOutputStream();
        byte[] v1 = new byte[1024];
        byte[] v4 = new byte[((BufferedBlockCipher)v0).getOutputSize(v1.length)];
        while(true) {
            int v3 = v8.read(v1);
            if(v3 <= -1) {
            	break;
            }

            v11 = ((BufferedBlockCipher)v0).processBytes(v1, 0, v3, v4, 0);
            if(v11 <= 0) {
                continue;
            }

            v10.write(v4, 0, v11);
        }

        v11 = ((BufferedBlockCipher)v0).doFinal(v4, 0);
        if(v11 > 0) {
            v10.write(v4, 0, v11);
        }

        return v10.toByteArray();
    }
}

