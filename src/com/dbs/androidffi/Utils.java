// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;

import org.apache.http.conn.util.InetAddressUtils;

import com.kony.crypto.util.CryptoConstants;

public class Utils {
    public Utils() {
        super();
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder v2 = new StringBuilder();
        int v0 = 0;
        while(v0 < bytes.length) {
        	 int v1 = bytes[v0] & 255;
             if(v1 < 16) {
            	 v2.append("0");
             }
             v2.append(Integer.toHexString(v1).toUpperCase(Locale.getDefault()));
             ++v0;
        }

        
        return v2.toString();
    }

    public static String getIPAddress(boolean useIPv4) {
        String v6 = null;
        try {
            Iterator<NetworkInterface> v7_1 = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();

            if(v7_1.hasNext()) {
            	Iterator<InetAddress> v8 = Collections.list(v7_1.next().getInetAddresses()).iterator();

                while(v8.hasNext()) {
                	InetAddress v0 = v8.next();
                    if(v0.isLoopbackAddress()) {
                        continue;
                    }
                    
                    v6 = v0.getHostAddress().toUpperCase(Locale.getDefault());
                    boolean v5 = InetAddressUtils.isIPv4Address(v6);
                    if(!useIPv4) {
                        continue;
                    }

                    if(!v5) {
                        continue;
                    }

                    break;
                }


            int v2 = v6.indexOf(37);
            if(v2 >= 0) {
            	v6 = v6.substring(0, v2);
            }
            }
            

        }
        catch(Exception v7) {
        }

        return v6;
    }


    public static String getMACAddress(String interfaceName) {
        try {
            Iterator<NetworkInterface> r5_Iterator = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
            while (r5_Iterator.hasNext()) {
                NetworkInterface intf = r5_Iterator.next();
                if (interfaceName != null) {
                    if (intf.getName().equalsIgnoreCase(interfaceName)) {
                    }
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) {
                    return CryptoConstants.EMPTY;
                }
                StringBuilder buf = new StringBuilder();
                int idx = 0;
                while (idx < mac.length) {
                    Object[] r6_ObjectA = new Object[1];
                    r6_ObjectA[0] = Byte.valueOf(mac[idx]);
                    buf.append(String.format("%02X:", r6_ObjectA));
                    idx++;
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        } catch (Exception e) {
        }
        return CryptoConstants.EMPTY;
    }

    public static byte[] getUTF8Bytes(String str) {
        byte[] v1;
        try {
            v1 = str.getBytes("UTF-8");
        }
        catch(Exception v0) {
            v1 = null;
        }

        return v1;
    }

    public static String loadFileAsString(String filename) throws IOException {
        String v7_1;
        int v6;
        int v3 = 0;
        boolean v5 = false;
        byte[] v2 = new byte[1024];
        ByteArrayOutputStream v1 = new ByteArrayOutputStream(1024);
        BufferedInputStream v4 = new BufferedInputStream(new FileInputStream(filename), 1024);
        
        try {
        	do
        	{
        		v6 = v4.read(v2);
        		if(v6 != -1) {
        	        if(v3 == 0) {
        	                if(v2[0] != -17 || v2[1] != -69 || v2[2] != -65) {
        	                    v1.write(v2, 0, v6);
        	                }
        	                else {
        	                    v5 = true;
        	                    v1.write(v2, 3, v6 - 3);
        	                }

        	                v3 += v6;

        	                /*if(v5) {
        	                    v7_1 = new String(v1.toByteArray(), "UTF-8");
        	                }
        	                else {
        	                    v7_1 = new String(v1.toByteArray());
        	                }*/


        		}
        	    else {
        	            v1.write(v2, 0, v6);
        	    }
        	        	v3 += v6;    
        		}
        		else
        			break;
   
        	  } while(true);
        	
        	if(v5) {
        	                    v7_1 = new String(v1.toByteArray(), "UTF-8");
        	                }
        	                else {
        	                    v7_1 = new String(v1.toByteArray());
        	                }
        }


        	catch(Throwable v7) {
        	            v4.close();
        		throw (IOException) v7;
        	        }
        	v4.close();
        	return v7_1;
    }
}

