// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.HashMap;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

public class RSAKeyProducer {
    private static Key privateKey;
    private static String publicKey;

    public RSAKeyProducer() {
        super();
    }

    public static BigInteger RSAPublicKeyExponent(RSAPublicKeySpec pub) {
        return pub.getPublicExponent();
    }

    public static String RSAPublicKeyExponentStr(RSAPublicKeySpec pub) {
        return pub.getPublicExponent().toString();
    }

    public static BigInteger RSAPublicKeyModulus(RSAPublicKeySpec pub) {
        return pub.getModulus();
    }

    public static String RSAPublicKeyModulusStr(RSAPublicKeySpec pub) {
        return pub.getModulus().toString();
    }

    public static void decrypt(String strToDecrypt) {
        try {
            System.out.println("plain : " + new String(Cipher.getInstance("RSA/None/PKCS1Padding", "BC").doFinal(Base64.decodeBase64(strToDecrypt))));
        }
        catch(Exception v3) {
        }
    }

    public static String encrypt(String modulus, String exponent, String strToEncrypt) {
        String v3;
        try {
            PublicKey v9 = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(RSAKeyProducer.getBigIntFromStr(modulus), RSAKeyProducer.getBigIntFromStr(exponent)));
            byte[] v6 = strToEncrypt.getBytes();
            Cipher v0 = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
            v0.init(1, ((Key)v9));
            byte[] v1 = v0.doFinal(v6);
            System.out.println("cipher: " + new String(v1));
            v3 = Base64.encodeBase64String(v1);
        }
        catch(Exception v2) {
            v3 = "";
            v2.printStackTrace();
        }

        return v3;
    }

    public static HashMap<String,Key> generate2048KeyPair() {
        HashMap<String,Key> v2;
        try {
            KeyPairGenerator v1 = KeyPairGenerator.getInstance("RSA", "BC");
            v1.initialize(2048);
            KeyPair v3 = v1.generateKeyPair();
            PublicKey v5 = v3.getPublic();
            PrivateKey v4 = v3.getPrivate();
            v2 = new HashMap<String,Key>();
            v2.put("PublicKey", v5);
            v2.put("PrivateKey", v4);
            RSAKeyProducer.setPrivateKey(((Key)v4));
        }
        catch(Exception v0) {
            v2 = null;
            v0.printStackTrace();
        }

        return v2;
    }

    public static RSAPublicKeySpec generateRSAPublicKeySpec(Key key) {
        RSAPublicKeySpec v2_1;
        try {
            v2_1 = KeyFactory.getInstance("RSA").getKeySpec(key, RSAPublicKeySpec.class);
        }
        catch(Exception v0) {
            v2_1 = null;
            v0.printStackTrace();
        }

        return v2_1;
    }

    public static BigInteger getBigIntFromStr(String str) {
        return new BigInteger(str);
    }

    public static Key getPrivateKey() {
        return RSAKeyProducer.privateKey;
    }

    public static String getPublicKey() {
        return RSAKeyProducer.publicKey;
    }

    public static String getStrFromBigInt(BigInteger bigInt) {
        return bigInt.toString();
    }

    public static void keyProducer(HashMap<String,Key> arg27) {
        try {
            Object v22 = arg27.get("PublicKey");
            Object v18 = arg27.get("PrivateKey");
            RSAPublicKeySpec v21 = RSAKeyProducer.generateRSAPublicKeySpec(((Key)v22));
            RSAPublicKeySpec v11 = new RSAPublicKeySpec(RSAKeyProducer.getBigIntFromStr(RSAKeyProducer.getStrFromBigInt(RSAKeyProducer.RSAPublicKeyModulus(v21))), RSAKeyProducer.getBigIntFromStr(RSAKeyProducer.getStrFromBigInt(RSAKeyProducer.RSAPublicKeyExponent(v21))));
            KeyFactory v8 = KeyFactory.getInstance("RSA");
            PublicKey v23 = v8.generatePublic(((KeySpec)v11));
            byte[] v10 = "Pratham is good boy".getBytes();
            Cipher v2 = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
            v2.init(1, v23);
            byte[] v3 = v2.doFinal(v10);
            System.out.println("cipher: " + new String(v3));
            v2.init(2, v8.generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(Base64.encodeBase64String(((Key)v18).getEncoded())))));
            System.out.println("plain : " + new String(v2.doFinal(v3)));
        }
        catch(Exception v4) {
            v4.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        RSAKeyProducer.keyProducer(RSAKeyProducer.generate2048KeyPair());
    }

    public static void publicKeyProducer(HashMap<String,Key> arg4) {
        Object v2 = arg4.get("PublicKey");
        arg4.get("PrivateKey");
        RSAKeyProducer.generateRSAPublicKeySpec(((Key)v2));
    }

    public static void setPrivateKey(Key privateKey) {
        RSAKeyProducer.privateKey = privateKey;
    }

    public static void setPublicKey(String publicKey) {
        RSAKeyProducer.publicKey = publicKey;
    }
}

