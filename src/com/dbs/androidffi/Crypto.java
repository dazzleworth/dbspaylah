// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import android.annotation.SuppressLint;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

@SuppressLint("DefaultLocale")
public class Crypto {
    public static final int SALT_LEN = 8;
    Cipher mDecipher;
    Cipher mEcipher;
    byte[] mInitVec;
    String mPassword;
    byte[] mSalt;

    public Crypto(String password) {
        super();
        this.mPassword = null;
        this.mInitVec = null;
        this.mSalt = null;
        this.mEcipher = null;
        this.mDecipher = null;
        this.mPassword = password;
    }

    private void Db(String msg) {
        System.out.println("** Crypt ** " + msg);
    }

    public void ReadEncryptedFile(File input, File output) throws IllegalBlockSizeException, BadPaddingException, IOException {
        long v6 = 0;
        byte[] v4 = new byte[1024];
        FileOutputStream v2 = new FileOutputStream(output);
        FileInputStream v1 = new FileInputStream(input);
        CipherInputStream v0 = new CipherInputStream(((InputStream)v1), this.mDecipher);
        while(true) {
            int v5 = v0.read(v4);
            if(v5 <= 0) {
            	break;
            }


            this.Db("read " + v5 + " bytes");
            v6 += ((long)v5);
            byte[] v8 = new byte[v5];
            int v3;
            for(v3 = 0; v3 < v5; ++v3) {
                v8[v3] = v4[v3];
            }

            v2.write(v8);
        }

        v2.flush();
        v0.close();
        v1.close();
        v2.close();
        this.Db("wrote " + v6 + " encrypted bytes");
    }

    public void WriteEncryptedFile(File input, File output) throws IOException, IllegalBlockSizeException, BadPaddingException {
        long v7 = 0;
        byte[] v4 = new byte[1024];
        FileOutputStream v2 = new FileOutputStream(output);
        FileInputStream v0 = new FileInputStream(input);
        while(true) {
            int v5 = v0.read(v4);
            if(v5 <= 0) {
                break;
            }

            this.Db("read " + v5 + " bytes");
            v7 += ((long)v5);
            byte[] v9 = new byte[v5];
            int v3;
            for(v3 = 0; v3 < v5; ++v3) {
                v9[v3] = v4[v3];
            }

            byte[] v6 = this.mEcipher.update(v9);
            if(v6 == null) {
                continue;
            }

            v2.write(v6);
        }

        byte[] v1 = this.mEcipher.doFinal();
        if(v1 != null) {
            v2.write(v1);
        }

        v2.flush();
        v0.close();
        v2.close();
        v2.close();
        this.Db("wrote " + v7 + " encrypted bytes");
    }

    public byte[] getInitVec() {
        return this.mInitVec;
    }

    public byte[] getSalt() {
        return this.mSalt;
    }

    @SuppressLint("DefaultLocale")
	public static void main(String[] args) {
        String v7 = null;
        String v6 = null;
        File v5 = new File("input.txt");
        File v4 = new File("encrypted.aes");
        File v1 = new File("decrypted.txt");
        Crypto v3 = new Crypto("mypassword");
        try {
            v3.setupEncrypt();
            v6 = Hex.encodeHexString(v3.getInitVec()).toUpperCase();
            v7 = Hex.encodeHexString(v3.getSalt()).toUpperCase();
        }
        catch(UnsupportedEncodingException v2) {
            v2.printStackTrace();
        }
        catch(BadPaddingException v2_1) {
            v2_1.printStackTrace();
        }
        catch(IllegalBlockSizeException v2_2) {
            v2_2.printStackTrace();
        }
        catch(InvalidParameterSpecException v2_3) {
            v2_3.printStackTrace();
        }
        catch(NoSuchPaddingException v2_4) {
            v2_4.printStackTrace();
        }
        catch(InvalidKeySpecException v2_5) {
            v2_5.printStackTrace();
        }
        catch(NoSuchAlgorithmException v2_6) {
            v2_6.printStackTrace();
        }
        catch(InvalidKeyException v2_7) {
            v2_7.printStackTrace();
        }

        try {
            v3.WriteEncryptedFile(v5, v4);
            System.out.printf("File encrypted to " + v4.getName() + "\niv:" + v6 + "\nsalt:" + v7 + "\n\n", new Object[0]);
        }
        catch(IOException v2_8) {
            v2_8.printStackTrace();
        }
        catch(BadPaddingException v2_1) {
            v2_1.printStackTrace();
        }
        catch(IllegalBlockSizeException v2_2) {
            v2_2.printStackTrace();
        }

        Crypto v0 = new Crypto("mypassword");
        try {
            v0.setupDecrypt(v6, v7);
        }
        catch(DecoderException v2_9) {
            v2_9.printStackTrace();
        }
        catch(InvalidAlgorithmParameterException v2_10) {
            v2_10.printStackTrace();
        }
        catch(NoSuchPaddingException v2_4) {
            v2_4.printStackTrace();
        }
        catch(InvalidKeySpecException v2_5) {
            v2_5.printStackTrace();
        }
        catch(NoSuchAlgorithmException v2_6) {
            v2_6.printStackTrace();
        }
        catch(InvalidKeyException v2_7) {
            v2_7.printStackTrace();
        }

        try {
            v0.ReadEncryptedFile(v4, v1);
            System.out.println("decryption finished to " + v1.getName());
        }
        catch(IOException v2_8) {
            v2_8.printStackTrace();
        }
        catch(BadPaddingException v2_1) {
            v2_1.printStackTrace();
        }
        catch(IllegalBlockSizeException v2_2) {
            v2_2.printStackTrace();
        }
    }

    public void setupDecrypt(String initvec, String salt) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, DecoderException {
        this.mSalt = Hex.decodeHex(salt.toCharArray());
        this.Db("got salt " + Hex.encodeHexString(this.mSalt));
        this.mInitVec = Hex.decodeHex(initvec.toCharArray());
        this.Db("got initvector :" + Hex.encodeHexString(this.mInitVec));
        SecretKeySpec v1 = new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(this.mPassword.toCharArray(), this.mSalt, 65536, 128)).getEncoded(), "AES");
        this.mDecipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        this.mDecipher.init(2, ((Key)v1), new IvParameterSpec(this.mInitVec));
    }

    public void setupEncrypt() throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeyException {
        this.mSalt = new byte[8];
        new SecureRandom().nextBytes(this.mSalt);
        this.Db("generated salt :" + Hex.encodeHexString(this.mSalt));
        SecretKeySpec v3 = new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(this.mPassword.toCharArray(), this.mSalt, 65536, 128)).getEncoded(), "AES");
        this.mEcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        this.mEcipher.init(1, ((Key)v3));
        this.mInitVec = this.mEcipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV();
        this.Db("mInitVec is :" + Hex.encodeHexString(this.mInitVec));
    }
}

