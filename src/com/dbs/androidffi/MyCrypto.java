// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class MyCrypto {
    public MyCrypto() {
        super();
    }

    public static String decryptWithAES256Key(String imei, Map<String, String> arg21) {
        String v9 = null;
        String v8 = "";
        try {
            String v13 = MyCrypto.sha256(imei);
            char[] v14 = v13.toCharArray();
            Object v11 = arg21.get("saltByteStr");
            System.out.println("The Salt String in decrypt method is : " + (((String)v11)));
            System.out.println("The SHA of imei in decrypt method is : " + v13);
            SecretKeySpec v12 = new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(v14, Base64.decodeBase64(((String)v11)), 65536, 256)).getEncoded(), "AES");
            Cipher v2 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            Object v7 = arg21.get("ivStr");
            Object v4 = arg21.get("ciphertextStr");
            System.out.println("The IVSTR in decrypted method is : " + (((String)v7)));
            System.out.println("The Encrypted string in decrypt method is : " + (((String)v4)));
            byte[] v6 = Base64.decodeBase64(((String)v7));
            byte[] v3 = Base64.decodeBase64(((String)v4));
            v2.init(2, ((Key)v12), new IvParameterSpec(v6));
            v9 = new String(v2.doFinal(v3), "UTF-8");
            System.out.println("The decrypted String is : " + v9);
            v8 = v9;
        }
        catch(InvalidAlgorithmParameterException v17) {
        	v8 = v9;
        }
        catch(UnsupportedEncodingException v17_1) {
        	v8 = v9;
        }
        catch(BadPaddingException v17_2) {
        	v8 = v9;
        }
        catch(IllegalBlockSizeException v17_3) {
        	v8 = v9;
        }
        catch(InvalidKeyException v17_4) {
        	v8 = v9;
        }
        catch(NoSuchPaddingException v17_5) {
        	v8 = v9;
        }
        catch(InvalidKeySpecException v17_6) {
        	v8 = v9;
        }
        catch(NoSuchAlgorithmException v17_7) {
        	v8 = v9;
        }

        return v8;
    }

    public static Map<String,String> encryptWithAES256Key(String imei) {
        HashMap<String,String> v5_1;
        HashMap<String,String> v6 = null;
        String v10;
        String v13;
        String v16;
        try {
            String v19 = MyCrypto.sha256(imei);
            char[] v20 = v19.toCharArray();
            byte[] v7 = Base64.decodeBase64(v19);
            new Random().nextBytes(v7);
            System.out.println("The SHA of imei in encrypt method is : " + v19);
            v16 = Base64.encodeBase64String(v7);
            System.out.println("The Salt String in encrypt method is : " + v16);
            SecretKeySpec v18 = new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1").generateSecret(new PBEKeySpec(v20, v7, 65536, 256)).getEncoded(), "AES");
            Cipher v8 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            v8.init(1, v18);
            byte[] v12 = v8.getParameters().getParameterSpec(IvParameterSpec.class).getIV();
            byte[] v9 = v8.doFinal("Hello, World!".getBytes("UTF-8"));
            v13 = Base64.encodeBase64String(v12);
            System.out.println("The IVSTR in encrypted method is : " + v13);
            v10 = Base64.encodeBase64String(v9);
            v6 = new HashMap<String,String>();
            v6.put("ivStr", v13);
            v6.put("ciphertextStr", v10);
            v6.put("saltByteStr", v16);
            System.out.println("The Encrypted String in Encryted method is : " + v10);
            v5_1 = v6;
        }

        catch(UnsupportedEncodingException v23) {
            v5_1 = v6;
        }
        catch(BadPaddingException v23_1) {
            v5_1 = v6;
        }
        catch(IllegalBlockSizeException v23_2) {
            v5_1 = v6;
        }
        catch(InvalidParameterSpecException v23_3) {
            v5_1 = v6;
        }
        catch(InvalidKeyException v23_4) {
            v5_1 = v6;
        }
        catch(NoSuchPaddingException v23_5) {
            v5_1 = v6;
        }
        catch(InvalidKeySpecException v23_6) {
            v5_1 = v6;
        }
        catch(NoSuchAlgorithmException v23_7) {
            v5_1 = v6;
        }

        return v5_1;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        System.out.println("The decrypted Plaintext in main method is : " + MyCrypto.decryptWithAES256Key("351823058568726", MyCrypto.encryptWithAES256Key("351823058568726")));
    }

    public static String sha256(String base) {
        try {
            byte[] v2 = MessageDigest.getInstance("SHA-256").digest(base.getBytes("UTF-8"));
            StringBuffer v4 = new StringBuffer();
            int v5 = 0;

            while(v5 < v2.length) {
            	String v3 = Integer.toHexString(v2[v5] & 0xFF);
                if(v3.length() == 1) {
                	v4.append('0');
                }
                v4.append(v3);
                ++v5;
                	
            }
            return v4.toString();
        }
        catch(Exception v1) {
            throw new RuntimeException(((Throwable)v1));
        }
    }
}

