// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

public abstract interface BinaryDecoder extends Decoder {
    public abstract byte[] decode(byte[] arg0) throws DecoderException;
}

