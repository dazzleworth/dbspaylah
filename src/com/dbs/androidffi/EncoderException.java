// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

public class EncoderException extends Exception {
    private static final long serialVersionUID = 1;

    public EncoderException() {
        super();
    }

    public EncoderException(String message) {
        super(message);
    }

    public EncoderException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncoderException(Throwable cause) {
        super(cause);
    }
}

