// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import com.konylabs.android.KonyMain;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;

public class RotateImageFFI {
    public RotateImageFFI() {
        super();
    }

    public static String bitmapTobase64(Bitmap b) {
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In the convert to base64 function");
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Bitmap in convert to base64");
        ByteArrayOutputStream v0 = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, ((OutputStream)v0));
        String v3 = Base64.encodeToString(v0.toByteArray(), 0);
        Log.e("rotate", "Rotated base64" + v3);
        b.recycle();
        return v3;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int v4 = options.outHeight;
        int v6 = options.outWidth;
        int v5 = 1;
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ original height ==" + v4);
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ original height ==" + v6);
        if((((double)v4)) > (((double)(v4 * reqWidth / v6))) || v6 > reqWidth) {
            int v2 = v4 / 5;
            int v3 = v6 / 5;
            while(v2 / v5 > reqHeight) {
                if(v3 / v5 <= reqWidth) {
                    break;
                }

                v5 *= 2;
            }
        }

        return v5;
    }

    private String getRealPathFromURI(Uri contentURI) {
        String v0;
        Cursor v7 = KonyMain.getActivityContext().getContentResolver().query(contentURI, null, null, null, null);
        if(v7 == null) {
            v0 = contentURI.getPath();
        }
        else {
            v7.moveToFirst();
            v0 = v7.getString(v7.getColumnIndex("_data"));
        }

        return v0;
    }

    public int getRotaionAngle(String imageUri) {
        int v4;
        int v5 = 0;
        try {
            KonyMain v0 = KonyMain.getActivityContext();
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In FFI" + imageUri);
            Uri v6 = Uri.parse(imageUri);
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ after parsing string to uri" + v6);
            ((Context)v0).getContentResolver().notifyChange(v6, null);
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ opening the file");
            File v3 = new File(this.getRealPathFromURI(v6));
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ after Path== " + this.getRealPathFromURI(v6));
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ after file open file path ==  " + v3.getAbsolutePath());
            v4 = new ExifInterface(this.getRealPathFromURI(v6)).getAttributeInt("Orientation", 1);
            switch(v4) {
                case 3: {
                    v5 = 180;
                    Log.v("Rotate", "Exif orientation: " + v4);
                }
                case 6: {
                    v5 = 90;
                    Log.v("Rotate", "Exif orientation: " + v4);
                }
                case 8: {
                    v5 = 270;
                    Log.v("Rotate", "Exif orientation: " + v4);
                }
            }
        }
        catch(Exception v1) {
        	v1.printStackTrace();
        }


        return v5;
    }

    public static Bitmap rotate(Bitmap b, int angle) {
        float v3 = 2f;
        int v8 = angle;
        if(b != null) {
            Matrix v5 = new Matrix();
            Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ In the rotate Image function");
            v5.setRotate(((float)v8), (((float)b.getWidth())) / v3, (((float)b.getHeight())) / v3);
            try {
                Bitmap v7 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), v5, true);
                Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Creating  bitmap in rotate function");
                if(b != v7) {
                    b.recycle();
                    b = v7;
                }
            }
            catch(OutOfMemoryError v9) {
                throw v9;
            }
        }

        return b;
    }

    public static String rotateFFI(String base64, String imageUri) {
        byte[] v1 = Base64.decode(base64, 0);
        BitmapFactory.Options v3 = new BitmapFactory.Options();
        v3.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(v1, 0, v1.length, v3);
        v3.inSampleSize = RotateImageFFI.calculateInSampleSize(v3, 120, 160);
        v3.inJustDecodeBounds = false;
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Creating initial bitmap");
        Bitmap v0 = BitmapFactory.decodeByteArray(v1, 0, v1.length, v3);
        int v6 = new RotateImageFFI().getRotaionAngle(imageUri);
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Calling rotate image function angle==" + v6);
        Bitmap v5 = RotateImageFFI.rotate(v0, v6);
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ After rotate bitmap function");
        Log.e("Rotate", "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Calling convert to base64 function");
        String v4 = RotateImageFFI.bitmapTobase64(v5);
        v0.recycle();
        v5.recycle();
        return v4;
    }
}

