// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;

public class RSA2048CryptoGram {
    public RSA2048CryptoGram() {
        super();
    }

    public static void main(String[] args) throws Exception {
        KeyPairGenerator v9 = KeyPairGenerator.getInstance("RSA", "BC");
        v9.initialize(2048);
        KeyPair v15 = v9.generateKeyPair();
        PublicKey v20 = v15.getPublic();
        PrivateKey v18 = v15.getPrivate();
        KeyFactory v7 = KeyFactory.getInstance("RSA");
        KeySpec v19 = v7.getKeySpec(v20, RSAPublicKeySpec.class);
        v7.getKeySpec(v18, RSAPrivateKeySpec.class);
        PublicKey v21 = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(new BigInteger(((RSAPublicKeySpec)v19).getModulus().toString()), new BigInteger(((RSAPublicKeySpec)v19).getPublicExponent().toString())));
        byte[] v10 = "Pratham is good boy".getBytes();
        Cipher v2 = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
        v2.init(1, v21);
        byte[] v3 = v2.doFinal(v10);
        System.out.println("cipher: " + new String(v3));
        v2.init(2, v18);
        System.out.println("plain : " + new String(v2.doFinal(v3)));
    }

    public static String sha256(String base) {
        try {
            byte[] v2 = MessageDigest.getInstance("SHA-256").digest(base.getBytes("UTF-8"));
            StringBuffer v4 = new StringBuffer();
            int v5;
            for(v5 = 0; v5 < v2.length; ++v5) {
                String v3 = Integer.toHexString(v2[v5] & 255);
                if(v3.length() == 1) {
                    v4.append('0');
                }

                v4.append(v3);
            }

            return v4.toString();
        }
        catch(Exception v1) {
            throw new RuntimeException(((Throwable)v1));
        }
    }
}

