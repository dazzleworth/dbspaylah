// Decompiled by JEB v1.4.201311050

package com.dbs.androidffi;

import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

public class RSAClient {
    public RSAClient() {
        super();
    }

    public String encrypt(String modulus, String exponent, String strToEncrypt) {
        String v3;
        try {
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@Build_5");
            PublicKey v9 = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(this.getBigIntFromStr(modulus), this.getBigIntFromStr(exponent)));
            byte[] v6 = strToEncrypt.getBytes();
            Cipher v0 = Cipher.getInstance("RSA/None/PKCS1Padding", "BC");
            System.out.println("Cipher Algorithm= " + v0.getAlgorithm());
            v0.init(1, ((Key)v9));
            byte[] v1 = v0.doFinal(v6);
            System.out.println("cipher_build_5: " + new String(v1));
            v3 = new String(Base64.encodeBase64(v1));
        }
        catch(Exception v2) {
            v3 = "";
            System.out.println("Exception occured");
            v2.printStackTrace();
        }

        return v3;
    }

    public BigInteger getBigIntFromStr(String str) {
        return new BigInteger(str);
    }

    public static void main(String[] args) {
        System.out.println("encText==" + new RSAClient().encrypt("17245692649748215159778349948453216529591972870923748223900326912123898897055229593444563372469402515564172297816952681177961841368458514090831932892573927723162928035001341407401595739736565330381516862687741957004852279377748849912956289487611573022392876327164698888676830510459545455907721352788238730307019261003121368606202834524651513123462421620710240055000150054091369943107726447591106659845635136362407312871778918515499722874086455689938724787114152415862091426761172724803197496000685251025313759046251437988553146146624340318622670342844157863939943349475845348090808901988245772846530654444001275232751", "17", "prathamesh"));
    }
}

