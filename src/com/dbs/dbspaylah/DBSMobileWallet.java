// Decompiled by JEB v1.4.201311050

package com.dbs.dbspaylah;

import android.os.Bundle;
import com.konylabs.android.KonyMain;

public class DBSMobileWallet extends KonyMain {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4284456677429593563L;
	private static DBSMobileWallet context;

    public DBSMobileWallet() {
        super();
        
    }

    public static DBSMobileWallet getActivityContext() {
        return DBSMobileWallet.context;
    }

    public int getAppSourceLocation() {
        return 1;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DBSMobileWallet.context = this;
    }
}

